We'll use folder m2p("1_","14_")l14 as an example. This is m=2, so layer two
where the path is 14th in the first layer, 1 in the second (one of these weeks
I'll deal with this backwards convention, sorry about that). l14 means we're at
location 14. For each layer, there are 3 points I tried to fit: 5, halfway, and
5 from the end.

Inside the folder are several plots and one jld2 file. The jld2 file contains
res, which gives a list of Optim.jl objects. What I did to fit the coordinate
was start at a value of 1, and increase exponentially up to the norm of that
layer (this is the normalized version, where each layer is normalized to thepp
number of paths). The order of the results in res corresponds to this
increasing value. This variable contains all of the information in the plots
(the specific commands are in the accompanying makeFiguresForEachCoordinate.jl).

This explains the somewhat odd plots in errorDescent; each red point is when we
have increased the target objective. The previous fit is used as a start for
the next fit, as in LASSO.

ActuallyFitPlot gives the minimizer which had the largest scattering
coefficient at the correct location (generally this is the last one). The
actual values vary quite a bit in magnitude between different examples; I only
the norm penalty on the vector as a small part of the objective, so the values
can be quite large. There are two plots here, the space domain and the positive
half of the Fourier domain.

Finally, there are firstLayerPlot and secondLayerPlot. I think firstLayerPlot
is generally self-evident as a (sqrt of) a scaleogram.

secondLayerPlot is somewhat harder to read, but is the style of plot we talked
about last week. The color gives the l₂ norm of that location, while the little
plot gives the location information there.

There are some results which ended up just continuing to be constant. This is
definitely not supposed to happen and will require some investigation. These
will have missing plots in secondLayerPlot



If you want to compare several plots, I included a short tex file that I was
using to compile comparisons.
