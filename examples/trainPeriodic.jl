using GLMNet
using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, FileIO, FFTW, LinearAlgebra
using MLJ, DataFrames, MLJLinearModels, LaTeXStrings
include("../simpleSignalExamples.jl")
N = 100
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generatePeriodicData(100), (128, 1, 300));
N=100
y = cat(["cylinder" for i = 1:100], ["Bell" for i = 1:100], ["Funnel" for i =
                                                             1:100], dims=1)
y = cat([[1 0 0] for i=1:100]..., [[0 1 0] for i=1:100]..., [[0 0 1] for i=1:100]..., dims=1)
St = scatteringTransform(size(data), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4,boundaryType=Periodic)
size(y)
@time stw = St(data)
X = ScatteringTransform.flatten(stw)'
size(X)
cons = ones(size(X,2),2) # if we want to constrain the coefficients to be non-negative
λ = 10. .^range(-7,1,length=100)
cvp = glmnetcv(X, y, Multinomial(), maxit=Int64(1e7), lambda=λ, parallel=true)
save("glmnetPeriodicCoefs1.jld2", "glmPathCV", cvp)
#shortPath = path # range from -5 to 1 and with step=.01 rather than -10 and
#length of 600
cvp.lambda
totLen = minimum(map(length, (cvp.lambda, cvp.meanloss, cvp.stdloss)))
# convergence path
plot(cvp.lambda[1:totLen], cvp.meanloss .+ cvp.stdloss, fill=cvp.meanloss .-
     cvp.stdloss,legend=:bottomright,scale=:log10, label="std")
plot!(cvp.lambda[1:totLen], cvp.meanloss,scale=:log10,label="mean loss")
vline!([cvp.lambda[chooseBestLambda(cvp)]],label=L"best\ \lambda", title="CV Lasso convergence")
savefig("GLMNetPeriodicConvergence.pdf"); savefig("GLMNetPeriodicConvergence.png")

sum(abs.(GLMNet.predict(cvp,X,outtype=:prob) - y))
argmax(GLMNet.predict(cvp,X,outtype=:prob)[1,:])
ŷ = map(eachslice(GLMNet.predict(cvp,X,outtype=:prob),dims=1)) do x
    ii =argmax(x)
    tmp=zeros(1,3); tmp[ii]=1
    return tmp
end
ŷ = cat(ŷ..., dims=1)
sum(ŷ-y)
yTest = cat([[1 0 0] for i=1:1000]..., [[0 1 0] for i=1:1000]..., [[0 0 1] for i=1:1000]..., dims=1)
testData = reshape(generateData(1000), (128, 1, 3000));
stBig = scatteringTransform(size(testData), cw=dog2, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4,boundaryType=Periodic)
resTest = ScatteringTransform.flatten(stBig(testData))
testŷ = GLMNet.predict(cvp,resTest',outtype=:prob)
sum(abs.(testŷ - yTest)) # net 20.22 error
ŷtest = map(eachslice(testŷ, dims=1)) do x
    ii =argmax(x)
    ii==1 ? "cylinder" : (ii==2 ? "Bell" : "Funnel")
end
ŷtest = coerce(ŷtest, OrderedFactor)
yTest = cat(["cylinder" for i = 1:1000], ["Bell" for i = 1:1000], ["Funnel" for i = 1:1000], dims=1)
yTest = coerce(yTest, OrderedFactor)
confusion_matrix(ŷtest,yTest) # it's always Funnel that gets mistaken for cylinder

# training convergence
besti = iλ
β = cvp.path.betas[:,:,besti]
rolledβ = ScatteringTransform.roll(β, St)

findall(abs.(rolledβ[1]) .> 1e-4)
findall(abs.(rolledβ[2]) .> 1e-8)
l = @layout [a{.2875w} b{.2875w} c{.425w}]
clims = (minimum(rolledβ[1]), maximum(rolledβ[1]))
plot(heatmap(rolledβ[1][:,:,1]', title=" \n Cylinder ", ylabel="scale",
             xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(rolledβ[1][:,:,2]', title="Dog2 first layer coefficients\nBell ",
             xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(rolledβ[1][:,:,3]', title=" \nFunnel ", xlabel="space",
             clims=clims, c=:viridis),
     layout=l)

nameThis = "glmnetFitSol."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
size(coefs[:,1][2])
plotSecondLayer(rolledβ[:,1], title="Cylinder coefficients Dog2",c=:viridis,logPower=false)
nameThis = "glmnetFitSol_layer2_cyl."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
plotSecondLayer(rolledβ[:,2], title="Bell coefficients Dog2",logPower=false,
                c=:viridis)
nameThis = "glmnetFitSol_layer2_bell."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
plotSecondLayer(rolledβ[:,3], title="Funnel coefficients Dog2",logPower=false,
                c=:viridis)
nameThis = "glmnetFitSol_layer2_fun."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
