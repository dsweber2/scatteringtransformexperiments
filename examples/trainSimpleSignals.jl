using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, FileIO, FFTW, LinearAlgebra
using MLJ, DataFrames, MLJLinearModels, LaTeXStrings
include("../simpleSignalExamples.jl")
N = 100
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
st = scatteringTransform(size(data), 2, β=[2,2,1], averagingLength=[2,3,4], outputPool=2 * 4)
st = scatteringTransform(size(data), cw=dog2, 2, β=[2,2,1], averagingLength=[2,3,4], outputPool=2 * 4)
c = getWavelets(st)
size(c[1])
heatmap(abs.(c[1]))
st = scatteringTransform(size(data), 2, β=[2,2,1], averagingLength=[2,3,4], outputPool=2 * 4)
stFlux
@time stw = st(data)
stw
plotSecondLayer(stw[:,1])
heatmap(stw[1][:,:,1]')
plot(data[:,1,1])
res = ScatteringTransform.flatten(stw)
size(res)


y = cat(["cylinder" for i = 1:100], ["Bell" for i = 1:100], ["Funnel" for i = 1:100], dims=1)
y = coerce(y, Multiclass)
St = scatteringTransform(size(data), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
@time stw = St(data)
res = ScatteringTransform.flatten(stw)
stBig = scatteringTransform(size(testData), cw=dog2, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4)
resTest = ScatteringTransform.flatten(stBig(testData))
yTest = cat(["cylinder" for i = 1:1000], ["Bell" for i = 1:1000], ["Funnel" for i = 1:1000], dims=1)
yTest = coerce(yTest, OrderedFactor)

MLJ.@load MultinomialClassifier verbosity = 1
tmpEx = MultinomialClassifier(penalty=:l1)#, solver=ProxGrad(max_iter=10000))
r = range(tmpEx, :lambda, lower=.1, upper=100, scale=:log)
selfTunerModel = TunedModel(model=tmpEx,
                            resampling=CV(nfolds=3),
                            tuning=Grid(resolution=10),
                            range=r, measure=cross_entropy)
selfTuner = machine(selfTunerModel, res', y)
MLJ.fit!(selfTuner)
report(selfTuner)
count(abs.(fitted_params(selfTuner).best_fitted_params.coefs) .> 1e-4)
selfTuner.params
fitted_params(selfTuner)
predicts = MLJ.predict(selfTuner, resTest')
(mode.(predicts))
size(yTest)
cm = confusion_matrix(mode.(predicts), yTest)
plting = selfTuner.report.plotting
d = sort(DataFrame(a=Float64.(plting.parameter_values)[:],
                   b=plting.measurements), :a)
plot(d.a,d.b,xscale=:log10)
selfTuner.report
selfTuner = selfTunerSparser
r = range(tmpEx, :lambda, lower=.1, upper=1000, scale=:log)
selfTunerModel = TunedModel(model=tmpEx,
                            resampling=CV(nfolds=3),
                            tuning=Grid(resolution=10),
                            range=r, measure=cross_entropy)
selfTunerSparser = machine(selfTunerModel, MLJ.table(res'), y)
selfTunerSparser = machine(selfTunerModel, res', y)
MLJ.fit!(selfTunerSparser)
report(selfTunerSparser)

sparsest=ScatteringTransform.roll(fitted_params(selfTunerSparser).best_fitted_params.coefs,St)
sparsest[2][:]
plot(sparsest[2][:,1,:])
plotSecondLayer(sparsest[:,1],title="Bell",logPower=false)
plotSecondLayer(sparsest[:,2],title="Bell",logPower=false)
plotSecondLayer(sparsest[:,3],title="Bell",logPower=false)
findall(abs.(sparsest[2]) .> 1e-4)
sparsest[2][:,10,7,1]
sparsest[2][7,3,12,2]
sparsest[2][1,7,5,3]
fitted_params(selfTunerSparser).best_fitted_params.coefs[end,:]
count(abs.(fitted_params(selfTunerSparser).best_fitted_params.coefs) .> 1e-4)
abs.(fitted_params(selfTunerSparser).best_fitted_params.coefs) .> 1e-4
findall(abs.(fitted_params(selfTunerSparser).best_fitted_params.coefs) .> 1e-4)
selfTunerSparser.params
fitted_params(selfTunerSparser)
predicts = MLJ.predict(selfTunerSparser, resTest')
(mode.(predicts))
size(yTest)
cm = confusion_matrix(mode.(predicts), yTest)
1-sum(diag(cm.mat))/3000
plting = selfTunerSparser.report.plotting
d = sort(DataFrame(a=Float64.(plting.parameter_values)[:],
                   b=plting.measurements), :a)
plot(d.a,d.b,xscale=:log10)
save("sparseLog2.jld2", "lambda100", selfTuner, "lambda1000", selfTunerSparser)
save("sparseLog2.jld2","lambda1000", selfTunerSparser)
function getCoefs(logitMachine)
    wat = fitted_params(selfTunerSparser).best_fitted_params.coefs
    cat([w[2] for w in wat]..., dims=2)'
end
getCoefs(selfTunerSparser)

# testing a single classification tree
MLJ.@load EvoTreeClassifier()
m = EvoTreeClassifier()
mac = machine(m, res', y)
fit!(mac)
mac
save("evoTreeModel.jld2", "evoMod",mac)
using EvoTrees
predicts = MLJ.predict(mac, resTest')
tmp = fitted_params(mac).fitresult
typeof(fitted_params(mac).fitresult.trees[1])
n = 3;plot(fitted_params(mac).fitresult.trees[n])
savefig("$(examps[1])/tree$(n).png")
examps
resTest
cm = confusion_matrix(mode.(MLJ.predict(mac, resTest')), yTest)
confusion_matrix(mode.(predict(mac,res')), y)
1-sum(diag(cm.mat)) / 3000

adr = 3624
res
fill(false, size(res,1))
t = ScatteringTransform.computeLoc(3624, res, St)
t.indices[1]==nothing
inBetween = ["$(x)00" for x in t.indices[3][1:end-2]]
"$(3)00$(inBetween...)"
function Int(t::pathLocs)
    for (ii, x) in enumerate(t.indices)
        if x!=nothing
            inBetween = ["$(y)00" for y in x[1:end-2]]
            return parse(Int, "$(ii-1)00$(inBetween...)")
        end
    end
end
Int(t)

# testing LDA
MLJ.@load LDA()
mlda = LDA()
mldac = machine(mlda, Float64.(res)', y)
fit!(mldac)
mldac
predicts = MLJ.predict(mldac, Float64.(resTest)')
cm = confusion_matrix(mode.(MLJ.predict(mldac, resTest')), yTest)
sum(diag(cm.mat))/3000
ldaMeans = roll(fitted_params(mldac).projected_class_means, St)
plot(ldaMeans[0][:,1,:],labels=["Bell" "Cylinder" "Funnel"],title="LDA layer zero means for $(examps[waveNumber])")
savefig("$(examps[waveNumber])/ldaAverages0.pdf"); savefig("$(examps[waveNumber])/ldaAverages0.png")
ldaMeans
l = @layout [a{.2875w} b{.2875w} c{.425w}]
waveNumber=1
clims = (minimum(ldaMeans[1]), maximum(ldaMeans[1]))
plot(heatmap(ldaMeans[1][:,:,3]', title=" \n Cylinder ", ylabel="scale", xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(ldaMeans[1][:,:,1]', title="$(examps[waveNumber]) lda means\nBell ", xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(ldaMeans[1][:,:,2]', title=" \nFunnel ", xlabel="space", clims=clims, c=:viridis),
     layout=l)
savefig("$(examps[waveNumber])/ldaAverages1.pdf"); savefig("$(examps[waveNumber])/ldaAverages1.png")
plotSecondLayer(ldaMeans[:,3],title="Cylinder lda mean")
savefig("$(examps[waveNumber])/secondldasCyl.pdf"); savefig("$(examps[waveNumber])/secondldasCyl.png")
plotSecondLayer(ldaMeans[:,1],title="Bell lda mean")
savefig("$(examps[waveNumber])/secondldasBell.pdf"); savefig("$(examps[waveNumber])/secondldasBell.png")
plotSecondLayer(ldaMeans[:,2],title="Funnel lda mean")
savefig("$(examps[waveNumber])/secondldasFun.pdf"); savefig("$(examps[waveNumber])/secondldasFun.png")
fitted_params(mldac)
fitted_params(mldac).projection_matrix
size(resTest)
save("lda_solution.jld2", "ldaFit", mldac)
# iterate over examps
simpleModel =
    MultinomialClassifier(penalty=:l1, lambda=1.0)
justCV = TunedModel(model=simpleModel,
                    range=range(simpleModel, :lambda, lower=.9, upper=1,
                                scale=:log), resampling=CV(nfolds=10, shuffle=true))

St = scatteringTransform(size(data), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
@time stw = St(data)
res = ScatteringTransform.flatten(stw)
y = cat(["cylinder" for i = 1:100], ["Bell" for i = 1:100], ["Funnel" for i = 1:100], dims=1)
y = coerce(y, OrderedFactor)
mc = machine(justCV, res', y)
nCv = 30
examps = (dog2, dog3, morl, paul2, cHaar, cDb2)
models = Array{typeof(mc)}(undef, length(examps))
sigSize = zeros(length(examps))
confMats = Array{typeof(confusion_matrix(y, y))}(undef, length(examps))
testData = reshape(generateData(1000), (128, 1, 3000));
yTest = cat(["cylinder" for i = 1:1000], ["Bell" for i = 1:1000], ["Funnel" for i = 1:1000], dims=1)
yTest = coerce(yTest, OrderedFactor)
for (ii, ex) in enumerate(examps)
    println("starting round $ii it is wavelets of type $ex")
    st = scatteringTransform(size(data), cw=ex, 2, β=[2,2,1],
                             averagingLength=[2,3,4], outputPool=2 * 4)
    @time stw = st(data)
    res = ScatteringTransform.flatten(stw)
    stBig = scatteringTransform(size(testData), cw=ex, 2, β=[2,2,1],
                             averagingLength=[2,3,4], outputPool=2 * 4)
    resTest = ScatteringTransform.flatten(stBig(testData))
    models[ii] = machine(justCV, res', y)
    sigSize[ii] = size(res, 1)
    MLJ.fit!(models[ii])
    predicts = MLJ.predict(models[ii], resTest')
    confMats[ii] = confusion_matrix(mode.(predicts), yTest) # test set
    println(confMats[ii])
end
using JLD2, FileIO
save("trainedl1Models.jld2", "models", models)
tmp = FileIO.load("trainedl1Models.jld2")
mods = tmp["models"]
k = 6; totalCoefs = prod(size(fitted_params(mods[k]).best_fitted_params.coefs))
nnz = count(abs.(fitted_params(mods[k]).best_fitted_params.coefs) .> 1e-4)
nnz/totalCoefs
confTrainMats = Array{typeof(confusion_matrix(y, y))}(undef, length(examps))
confMats = Array{typeof(confusion_matrix(y, y))}(undef, length(examps))
testData = reshape(generateData(1000), (128, 1, 3000));
yTest = cat(["cylinder" for i = 1:1000], ["Bell" for i = 1:1000], ["Funnel" for i = 1:1000], dims=1)
yTest = coerce(yTest, OrderedFactor)
minimum(abs.(testData))
for (ii, ex) in enumerate(examps)
    st = scatteringTransform(size(data), cw=ex, 2, β=[2,2,1],
                             averagingLength=[2,3,4], outputPool=2 * 4)
    orig = st(data)
    origPred = predict(mods[ii], ScatteringTransform.flatten(orig)')
    confTrainMats[ii] = confusion_matrix(mode.(origPred), y)
    stBig = scatteringTransform(size(testData), cw=ex, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4)
    resTest = ScatteringTransform.flatten(stBig(testData))
    predicts = MLJ.predict(mods[ii], resTest')
    confMats[ii] = confusion_matrix(mode.(predicts), yTest) # test set
    # train error
    println(confMats[ii])
end
k = 6; confTrainMats[k]
(sum(diag(confTrainMats[k].mat))/300, 1-sum(diag(confTrainMats[k].mat))/300)
confMats[k]
(sum(diag(confMats[k].mat))/3000, 1-sum(diag(confMats[k].mat))/3000)
(confMats[3])
confusion_matrix(mode.(predict(,res')), y)
for x in examps
    mkdir("$x")
end

models[1]
mc.best_model
predicts = MLJ.predict(mc, resTest')
mode.(predicts)
cm = confusion_matrix(mode.(predicts), y) # test set
fitted_params(mc).coefs

# visualizing the data
plot([mean(data[:,1,1:100], dims=2) mean(data[:,1,101:200], dims=2) mean(data[:,1,201:300], dims=2)],
     labels=["Cylinder" "Bell" "Funnel"])
savefig("rawSignalsAverageValues.pdf"); savefig("rawSignalsAverageValues.png")
waveNumber = 1
st = scatteringTransform(size(data), cw=examps[waveNumber], 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
@time res = st(data)
zerothLayerMeans = [mean(res[0][:,1,1:100], dims=2)[:,1,1] mean(res[0][:,1,101:200], dims=2)[:,1,1] mean(res[0][:,1,201:300], dims=2)[:,1,1]]
plot(zerothLayerMeans, labels=["Cylinder" "Bell" "Funnel"], title="$(examps[waveNumber]): zeroth layer averages")
savefig("$(examps[waveNumber])/zeroAverages.pdf"); savefig("$(examps[waveNumber])/zeroAverages.png")
firstLayerMeans = cat(mean(stw[1][:,:,1:100], dims=3)[:,:,1]',
                       mean(stw[1][:,:,101:200], dims=3)[:,:,1]',
                       mean(stw[1][:,:,201:300], dims=3)[:,:,1]',dims=3)
size(firstLayerMeans)
clims = (minimum(firstLayerMeans), maximum(firstLayerMeans))
l = @layout [a{.2875w} b{.2875w} c{.425w}]
plot(heatmap(firstLayerMeans[:,:,1], title=" \n Cylinder", ylabel="scale", xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(firstLayerMeans[:,:,2], title="$(examps[waveNumber]) Average\nBell", xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(firstLayerMeans[:,:,3], title=" \nFunnel", xlabel="space", clims=clims, c=:viridis),
     layout=l)
savefig("$(examps[waveNumber])/firstAverages.pdf"); savefig("$(examps[waveNumber])/firstAverages.png")
secondLayerMeans = cat(mean(res[2][:,:,:,1:100], dims=4),
     mean(res[2][:,:,:,101:200], dims=4), mean(res[2][:,:,:,201:300], dims=4), dims=4)
means = ScatteredOut((zerothLayerMeans, firstLayerMeans, secondLayerMeans))
plotSecondLayer(means[:,1],title="Cylinder average")
savefig("$(examps[waveNumber])/secondAveragesCyl.pdf"); savefig("$(examps[waveNumber])/secondAveragesCyl.png")
plotSecondLayer(means[:,2],title="Bell average")
savefig("$(examps[waveNumber])/secondAveragesBell.pdf"); savefig("$(examps[waveNumber])/secondAveragesBell.png")
plotSecondLayer(means[:,3],title="Funnel average")
savefig("$(examps[waveNumber])/secondAveragesFun.pdf"); savefig("$(examps[waveNumber])/secondAveragesFun.png")

# visualizing the results
coefs = roll(fitted_params(models[waveNumber]).coefs, st)
t = models[waveNumber]
clims = (minimum(coefs[1]), maximum(coefs[1]))
l = @layout [a{.2875w} b{.2875w} c{.425w}]
plot(heatmap(coefs[:,3][1]', title=" \n Cylinder coefficients", clims=clims, colorbar=false, c=:viridis),
     heatmap(coefs[:,1][1]', title="$(examps[waveNumber])\nBell coefficients", clims=clims, colorbar=false, c=:viridis),
     heatmap(coefs[:,2][1]', title=" \nFunnel coefficients ", clims=clims, c=:viridis),
     layout=l)
nameThis = "Beta221AL234Op24_coeff_layer1$(examps[waveNumber])."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
size(coefs[:,1][2])
plotSecondLayer(coefs[:,3], title="Cylinder coefficients $(examps[waveNumber])",logPower=true,
                c=cgrad(:viridis, [0,.9]))
nameThis = "Beta221AL234Op24_coeff_layer2_cyl_$(examps[waveNumber])."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
plotSecondLayer(coefs[:,1], title="Bell coefficients $(examps[waveNumber])",logPower=true,
                c=cgrad(:viridis, [0,.9]))
nameThis = "Beta221AL234Op24_coeff_layer2_bell_$(examps[waveNumber])."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
savefig("dog2Beta221AL234Op24_coeff_layer2_bell.pdf"); savefig("dog2Beta221AL234Op24_coeff_layer2_bell.png")
plotSecondLayer(coefs[:,2], title="Funnel coefficients $(examps[waveNumber])",logPower=true,
                c=cgrad(:viridis, [0,.9]))
nameThis = "Beta221AL234Op24_coeff_layer2_fun_$(examps[waveNumber])."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
savefig("dog2Beta221AL234Op24_coeff_layer2_fun.pdf"); savefig("dog2Beta221AL234Op24_coeff_layer2_fun.png")




using GLMNet
N=100
y = cat(["cylinder" for i = 1:100], ["Bell" for i = 1:100], ["Funnel" for i =
                                                             1:100], dims=1)
y = cat([[1 0 0] for i=1:100]..., [[0 1 0] for i=1:100]..., [[0 0 1] for i=1:100]..., dims=1)
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform(size(data), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
size(y)
@time stw = St(data)
X = ScatteringTransform.flatten(stw)'
size(X)
cons = ones(size(X,2),2) # if we want to constrain the coefficients to be non-negative
λ = 10. .^range(-7,0,length=600)
path = glmnetcv(X, y, Multinomial(), lambda=λ)
save("glmnetCoefs.jld2","glmPathCV", path)
#shortPath = path # range from -5 to 1 and with step=.01 rather than -10 and
#length of 600
path = glmPathCV
path.lambda
# convergence path
plot(path.lambda, path.meanloss .+ path.stdloss, fill=path.meanloss .-
     path.stdloss,legend=:bottomright,scale=:log10, label="std")
plot!(path.lambda, path.meanloss,scale=:log10,label="mean loss")
vline!([path.lambda[chooseBestLambda(path)]],label=L"best\ \lambda", title="CV Lasso convergence")
savefig("GLMNetConvergence.pdf"); savefig("GLMNetConvergence.png")
"""
    indLambda = chooseBestLambda(cv)
the best lambda is the one which is has the largest value of lambda still within
a stdloss of the minimal value. This returns the index, not the value of lambda
"""
function chooseBestLambda(cv)
    lambdamin(cv)
    argmin(cv.meanloss)
    minimum(cv.meanloss)
    findlast((cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss))
end

sum(abs.(GLMNet.predict(path,X,outtype=:prob) - y))
argmax(GLMNet.predict(path,X,outtype=:prob)[1,:])
ŷ = map(eachslice(GLMNet.predict(path,X,outtype=:prob),dims=1)) do x
    ii =argmax(x)
    tmp=zeros(1,3); tmp[ii]=1
    return tmp
end
ŷ = cat(ŷ..., dims=1)
sum(ŷ-y)
yTest = cat([[1 0 0] for i=1:1000]..., [[0 1 0] for i=1:1000]..., [[0 0 1] for i=1:1000]..., dims=1)
testData = reshape(generateData(1000), (128, 1, 3000));
stBig = scatteringTransform(size(testData), cw=dog2, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4)
resTest = ScatteringTransform.flatten(stBig(testData))
testŷ = GLMNet.predict(path,resTest',outtype=:prob)
sum(abs.(testŷ - yTest)) # net 20.22 error
ŷtest = map(eachslice(testŷ, dims=1)) do x
    ii =argmax(x)
    ii==1 ? "cylinder" : (ii==2 ? "Bell" : "Funnel")
end
ŷtest = coerce(ŷtest, OrderedFactor)
yTest = cat(["cylinder" for i = 1:1000], ["Bell" for i = 1:1000], ["Funnel" for i = 1:1000], dims=1)
yTest = coerce(yTest, OrderedFactor)
confusion_matrix(ŷtest,yTest) # it's always Funnel that gets mistaken for cylinder

# training convergence
besti = iλ
β = path.path.betas[:,:,besti]
rolledβ = ScatteringTransform.roll(β, St)

findall(abs.(rolledβ[1]) .> 1e-4)
findall(abs.(rolledβ[2]) .> 1e-8)
l = @layout [a{.2875w} b{.2875w} c{.425w}]
clims = (minimum(rolledβ[1]), maximum(rolledβ[1]))
plot(heatmap(rolledβ[1][:,:,1]', title=" \n Cylinder ", ylabel="scale",
             xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(rolledβ[1][:,:,2]', title="Dog2 first layer coefficients\nBell ",
             xlabel="space", clims=clims, colorbar=false, c=:viridis),
     heatmap(rolledβ[1][:,:,3]', title=" \nFunnel ", xlabel="space",
             clims=clims, c=:viridis),
     layout=l)

nameThis = "glmnetFitSol."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
size(coefs[:,1][2])
plotSecondLayer(rolledβ[:,1], title="Cylinder coefficients Dog2",c=:viridis,logPower=false)
nameThis = "glmnetFitSol_layer2_cyl."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
plotSecondLayer(rolledβ[:,2], title="Bell coefficients Dog2",logPower=false,
                c=:viridis)
nameThis = "glmnetFitSol_layer2_bell."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
plotSecondLayer(rolledβ[:,3], title="Funnel coefficients Dog2",logPower=false,
                c=:viridis)
nameThis = "glmnetFitSol_layer2_fun."
savefig(nameThis * "pdf"); savefig(nameThis * "png")
