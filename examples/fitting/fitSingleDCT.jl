using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
using Serialization
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,1], St)
plotSecondLayer(wrappedTarget)
null = St(zeros(128))
pathCollection = nonZeroPaths(wrappedTarget)
reusingOldStart = true; dampHighFrequency = true
## PathLoc to fit
loc = 4; firstP = 5; secondP = 7
p = pathLocs(2, (loc, secondP, firstP))
## PathLoc to fit
saveSerialName = "singleFits/weightedModel_DCT_f$(firstP)s$(secondP)l$(loc).tmp"
saveName = "singleFits/weighted12Hr_DCT_f$(firstP)s$(secondP)l$(loc).jld2"
null.output[3][loc,secondP,firstP,1] = 1f0
wrappedTarget = ScatteringTransform.normalize(null, 1)
distanceNorm = 1.0; elementNorm = 2.0
netWeight = ScatteringTransform.computeWeightMatrices(wrappedTarget, (p,), 1.0)
wObj = ScatteringTransform.weightByDistance(wrappedTarget, (p,), St, distanceNorm, elementNorm)
wObj(zeros(128))
P = plan_dct(zeros(128))
function FourierObj(x̂)
    x = P \ x̂
    wObj(x)
end
function saveSerial(name, object...)
    fh = open(name, "w")
    serialize(fh, object...)
    close(fh)
end

if reusingOldStart && isfile(saveSerialName)
    # reusing the whole setup
    fh = open(saveSerialName, "r")
    setProbW = deserialize(fh)
else
    setProbW = bbsetup(FourierObj; SearchRange=(-100., 100.), NumDimensions=128, PopulationSize=5000, MaxTime=10.0, CallbackFunction=callbackW, CallbackInterval=0.0, SaveTrace=true, NThreads=Threads.nthreads() - 1)
end

saveNameOld = "singleFits/weighted12Hr_f$(firstP)s$(secondP)l$(loc).jld2"
if reusingOldStart && (isfile(saveName) || isfile(saveNameOld))
    if isfile(saveName)
        d = load(saveName)
    else
        d = load(saveNameOld)
    end
    fitnessCallsHistoryW = d["fitnessCallsHistoryW"]
    fitnessHistoryW = d["fitnessHistoryW"]
    oldPop = d["individuals"]
    if dampHighFrequency
        scores = [FourierObj(x) for x in eachslice(oldPop, dims=2)]
        iSort = sortperm(scores)
        freqPop = ScatteringTransform.dampenAboveBestHardbandpass(oldPop, iSort, FourierObj, .1)
        ScatteringTransform.adjustPopulation!(setProbW, freqPop)
    end
else
    if dampHighFrequency
        error("there is no file with the previous fit?")
    end
    fitnessCallsHistoryW = Array{Int,1}()
    fitnessHistoryW = Array{Float64,1}()
end
callbackW = function recW(oc)
    push!(fitnessCallsHistoryW, num_func_evals(oc))
    push!(fitnessHistoryW, best_fitness(oc))
end
bboptimize(setProbW, MaxTime=30 * 60.0)
saveSerial(saveSerialName,setProbW)
for i = 1:2 * 12
    bboptimize(setProbW, MaxTime=30 * 60.0)
    saveSerial(saveSerialName, setProbW)
    save(saveName, "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW", fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals)
end
exit()
fnto(x) = fieldnames(typeof(x))
fnto(setProbW.problem)
fnto(setProbW.optimizer)
setProbW.optimizer.population
typeof(setProbW)
freqPop = dampenAboveBestHardbandpass(spacePop, iSort, .1)
freqPop[:,1]
adjustPopulation!(setProbW, freqPop)
scoreDamp = [FourierObj(x) for x in eachslice(freqPop[:,1:13:end], dims=2)]
histogram!(scoreDamp)
histogram(scores)
plotError(x,y::BlackBoxOptim.OptController,kwargs...) = plotError(x, y.optimizer.population.individuals;kwargs...)
plotError(1:128, setProbW)
typeof(setProbW)
