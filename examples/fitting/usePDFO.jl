using PyCall

pdfo = pyimport("pdfo")
f(x) = (1 - x[1])^2 + 100 * (x[2] - x[1]^2)^2
ff = py"""
def ff(x):
    return (1.0 -x[0])**2 + 100.0 * (x[1]-x[0]**2)**2
"""
pdfo.pdfo(ff,[0.0, 0.0])
py"ff"([1.9, 1.09])
