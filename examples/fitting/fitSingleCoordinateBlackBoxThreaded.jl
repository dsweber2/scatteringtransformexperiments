using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
using Serialization
function saveSerial(name, object...)
    fh = open(name, "w")
    serialize(fh, object...)
    close(fh)
end
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,1], St)
wrappedTarget = roll(β[:,1], St)
plotSecondLayer(wrappedTarget)
null = St(zeros(128))
pathCollection = nonZeroPaths(wrappedTarget)
reusingOldStart = true; dampHighFrequency = false
## PathLoc to fit
loc = 1; firstP = 25; secondP = 15
p = pathLocs(2, (loc, secondP, firstP))
## PathLoc to fit
saveSerialName = "singleFits/weightedModel_f$(firstP)s$(secondP)l$(loc).tmp"
saveName = "singleFits/weighted12Hr_f$(firstP)s$(secondP)l$(loc).jld2"
null.output[3][loc,secondP,firstP,1] = 1f0
wrappedTarget = ScatteringTransform.normalize(null, 1)

distanceNorm = 1.0; elementNorm = 2.0
wObj = ScatteringTransform.weightByDistance(wrappedTarget, (p,), St, distanceNorm, elementNorm)
gradObj(x) = wObj(x) + 1e4 * norm(diff(x), 2)

if reusingOldStart && isfile(saveName)
    d = load(saveName)
    fitnessCallsHistoryW = d["fitnessCallsHistoryW"]
    fitnessHistoryW = d["fitnessHistoryW"]
    if "fitnessGradHistoryW" in keys(d) # not all previous fits have a gradObj
        fitnessGradHistoryW = d["fitnessGradHistoryW"]
    else
        fitnessGradHistoryW = Array{Float64,1}()
    end
    oldPop = d["individuals"]
else
    fitnessCallsHistoryW = Array{Int,1}()
    fitnessHistoryW = Array{Float64,1}()
    fitnessGradHistoryW = Array{Float64,1}()
end
callbackW = function recW(oc)
    push!(fitnessCallsHistoryW, num_func_evals(oc))
    justwObj = minimum([wObj(x) for x in eachslice(oc.optimizer.population.individuals, dims=2)])
    push!(fitnessHistoryW, justwObj)
    push!(fitnessHistoryW, best_fitness(oc))
end
setProbW = bbsetup(gradObj; SearchRange=(-10., 10.), NumDimensions=128, PopulationSize=5000, MaxTime=30 * 60.0, CallbackFunction=callbackW, CallbackInterval=180.0, SaveTrace=true)
bboptimize(setProbW)
BlackBoxOptim.run!(setProbW)
for i = 1:2 * 18
    BlackBoxOptim.run!(setProbW)
    save("singleFits/weighted12Hr_f$(firstP)s$(secondP)l$(loc).jld2", "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW", fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals, "fitnessGradHistoryW", fitnessGradHistoryW)
    saveSerial(saveSerialName, setProbW)
end
exit()
bc = best_candidate(setProbW.runcontrollers[end])
wObj(bc)
wObj(bc / 100) - wObj(bc)
using DSP
f = [1, 2, 3, 4, 3, 2, 1]; f = f ./ norm(f)
smoothed = DSP.conv(bc, f)[4:end - 3]; wObj(smoothed)
plot([bc, smoothed])
butFilt = filt(analogfilter(Lowpass(.4), Elliptic(4, 0.9, 30)), bc); plot([abs.(rfft(smoothed)), abs.(rfft(bc)), abs.(rfft(butFilt))], labels=["smooth" "bc" "filtered"])
(wObj(butFilt))
cosine(128)
plot(smoothed)
size(DSP.conv(bc, [1, 2, 1]))
plot(bc)
plot(abs.(rfft(bc)))
plotSecondLayer(St(bc))
plotSecondLayer(wrappedTarget)
plot([bc, data[:,:,1]])
plotSecondLayer(St(data[:,:,1]))
firstEx = St(data[:,:,1])
plot([firstEx[0] St(bc)[0]])
St(data[:,:,1]) .- St(bc)
