using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using ScatteringTransform
using Test
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)


# initial dummy experiment
target = St(data[:,:,1:1])
objdummy = makeObjFun(target, St, false, 1e-0, true, false)
objD(x) = Float64(objdummy(x, St(data[:,:,1:1])))
objD(data[:,:,1:1]), objD(rand(128))
searchMax = norm(data[:,:,1:1], Inf)
res = bboptimize(objD; SearchRange=(-searchMax, searchMax),
                 NumDimensions=128, MaxTime=5 * 60.0, SaveTrace=true)
compare_optimizers(x -> Float64(obj(x)); SearchRange=(-searchMax, searchMax),
                 NumDimensions=128, SaveTrace=true)
res
res.archive_output
best_fitness(res)
plot([data[:,:,1] best_candidate(res)])
fieldnames(typeof(res.method_output))
fieldnames(typeof(res.method_output.population))
fieldnames(typeof(res.method_output.population))
res.method_output.population
res.f_calls
res.method
res.fit_scheme
fieldnames(typeof(res.archive_output))
res.archive_output.best_fitness
plot(res)
res.parameters
(:method, :stop_reason, :iterations, :start_time, :elapsed_time, :parameters, :f_calls, :fit_scheme, :archive_output, :method_output)
fieldnames(typeof(res))
obj(randn(128))
bboptimize(res)
res isa BlackBoxOptim.OptController
typeof(res)

# fitting each separately
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
target = βplus[:,2] * norm(St(randn(128)))
objxy = makeObjFun(target, St, false, 1e-0, true, false)
obj(x) = objxy(x, target)
obj(randn(128))
searchMax = norm(data[:,:,1:1], Inf)
fitnessCallsHistory = Array{Int,1}()
fitnessHistory = Array{Float64,1}()
callback = function rec(oc)
    push!(fitnessCallsHistory, num_func_evals(oc))
    push!(fitnessHistory, best_fitness(oc))
end
setProb = bbsetup(x -> Float64(obj(x)); SearchRange=(-searchMax, searchMax),
                  NumDimensions=128, PopulationSize=5000, MaxTime=60.0,
                  CallbackFunction=callback, CallbackInterval=0.0, SaveTrace=true)
BlackBoxOptim.run!(setProb)
plot(fitnessCallsHistory, fitnessHistory)
typeof(setProb)
fieldnames(typeof(setProb))
function updateObjective(setProb, newObj)
    newSetProb = deepcopy(setProb)
    newSetProb.problem.objfunc = newObj
    newSetProb.runcontrollers[end].evaluator.problem.objfunc = newObj
    return newSetProb
end
newProb.problem.objfunc(randn(128))
lastrun(newProb).evaluator.problem.objfunc(randn(128))
setProb.problem.objfunc(randn(128))
typeof(newProb.runcontrollers[end])
fieldnames(typeof(lastrun(newProb)))
lastrun(newProb).callback_function
fieldnames(typeof(newProb.runcontrollers[end].evaluator.problem))
newProb = updateObjective(setProb, x -> Float64(objxy(x, interimTarget)));
BlackBoxOptim.run!(newProb)
best_fitness(newProb)




res = bboptimize(x -> Float64(obj(x)); SearchRange=(-searchMax, searchMax),
                 NumDimensions=128, MaxTime=2 * 60.0, SaveTrace=true)
plotSecondLayer(St(best_candidate(res)))
plotSecondLayer(roll(target, St))

objxy = makeObjFun(target, St, false, 1e-0, true, false)
objxy(data[:,:,1], target)
effectiveTarget = target * norm(St(data[:,:,1]))
interimTarget = effectiveTarget
res2 = bboptimize(x -> Float64(obj(x, interimTarget)); SearchRange=(-searchMax, searchMax),
                  NumDimensions=128, PopulationSize=1000, MaxTime=3 * 60.0, SaveTrace=true)
# set the target to be halfway between
solSTDomain = ScatteringTransform.flatten(St(best_candidate(res2)))

function geometricMean(x...)
    if all(x .> 0)
        return exp(mean(log, x)) # geometric mean when it's well defined
    else
        return mean(x)           # arithmetic
    end
end

plot([map(geometricMean, effectiveTarget, solSTDomain, interimTarget), effectiveTarget, solSTDomain])
N = 10
interimTargets = zeros(size(target, 1), N)
interimTargets[:,1] = target * norm(St(data[:,:,1]))
firstRun = bboptimize(x -> Float64(obj(x, interimTarget)); SearchRange=(-searchMax, searchMax),
                      NumDimensions=128, PopulationSize=1000, MaxTime=3 * 60.0, SaveTrace=true)
listOfRes = Array{typeof(res3)}(undef, N)
listOfRes[1] = firstRun
for ii = 2:N
    solSTDomain = ScatteringTransform.flatten(St(best_candidate(listOfRes[ii - 1])))
    interimTarget = map(geometricMean, effectiveTarget, solSTDomain, interimTarget)

    listOfRes[ii] = bboptimize(x -> Float64(obj(x, interimTarget)); SearchRange=(-searchMax, searchMax),
                      NumDimensions=128, PopulationSize=1000, MaxTime=1 * 60.0, SaveTrace=true)
end
plot(map(best_fitness, listOfRes))
effectiveTarget
solSTDomain = ScatteringTransform.flatten(St(best_candidate(listOfRes[end])))
