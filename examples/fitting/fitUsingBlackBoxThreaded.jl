using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,2], St)
wrappedTarget = ScatteringTransform.normalize(wrappedTarget, 1)
correctlyNormalized = ScatteringTransform.flatten(wrappedTarget)
target = βplus[:,2] * norm(St(randn(128))) # adjust the norms so that it isn't
# just pulling everything to zero
objxy = makeObjFun(target, St, false, 1e-0, true, false)
obj(x) = objxy(x, target)
searchMax = norm(data[:,:,1:1], Inf)
fitnessCallsHistory = Array{Int,1}()
fitnessHistory = Array{Float64,1}()
callback = function rec(oc)
    push!(fitnessCallsHistory, num_func_evals(oc))
    push!(fitnessHistory, best_fitness(oc))
end
setProb = bbsetup(x -> Float64(obj(x)); SearchRange=(-searchMax, searchMax), NumDimensions=128, PopulationSize=5000, MaxTime=60.0, SaveTrace=true)
BlackBoxOptim.run!(setProb)
maxTime = 13 * 60 * 60
resMulti = bbsetup(x -> Float64(obj(x)); SearchRange=(-searchMax, searchMax), NumDimensions=128, PopulationSize=10000, MaxTime=maxTime, CallbackFunction=callback, CallbackInterval=0.0, SaveTrace=true, NThreads=Threads.nthreads() - 1)
save("tmpRes.jld2", "resMulti", resMulti, "fitnessCallsHistory",
     fitnessCallsHistory, "fitnessHistory", fitnessHistory)
exit()

JLD2.@load "tmpRes.jld2"
resMulti
plot(fitnessCallsHistory, fitnessHistory, yscale=:log10)
fieldnames(typeof(resMulti))
typeof(resMulti.method_output)
plot(best_candidate(resMulti),legend=false,title="actually trained")
solST = St(best_candidate(resMulti))
solSTDomain = ScatteringTransform.flatten(solST)
plot([solSTDomain, target, correctlyNormalized], labels=["fit solution" "target funnel weights" "correct norm"], title="all of the coefficients in a vector")
plot([solSTDomain correctlyNormalized])
wrappedTarget = roll(target, St)
heatmap(solST[1][:,:,1])
plot(wrappedTarget[0][:, 1])
heatmap(wrappedTarget[1][:,:,1])
heatmap(wrappedTarget[1])
plotSecondLayer(solST)
plotSecondLayer(wrappedTarget)
