using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,1], St)
null = St(zeros(128))
pathCollection = nonZeroPaths(wrappedTarget)
## PathLoc to fit
loc = 1; firstP = 25; secondP = 5
p = pathLocs(2, (loc, secondP, firstP))
null.output[3][loc,secondP,firstP,1] = 1f0
wrappedTarget = ScatteringTransform.normalize(null, 1)
obj(x) = Float64(norm(wrappedTarget[2] - St(x)[2])) # penalizing just the second layer
fitnessCallsHistoryW = Array{Int,1}()
fitnessHistoryW = Array{Float64,1}()
callbackW = function recW(oc)
    push!(fitnessCallsHistoryW, num_func_evals(oc))
    push!(fitnessHistoryW, best_fitness(oc))
end
setProbW = bbsetup(obj; SearchRange=(-10., 10.), NumDimensions=128, PopulationSize=5000, MaxTime=30 * 60.0, CallbackFunction=callbackW, CallbackInterval=0.0, SaveTrace=true)
BlackBoxOptim.run!(setProbW)
for i = 1:2 * 18
    BlackBoxOptim.run!(setProbW)
end
save("singleFits/18Hr_f$(firstP)s$(secondP)l$(loc).jld2", "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW",fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals)
