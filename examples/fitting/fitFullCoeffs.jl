using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,2], St)
wrappedTarget = ScatteringTransform.normalize(wrappedTarget, 1)
distanceNorm = 1.0; elementNorm = 2.0
pathCollection = nonZeroPaths(wrappedTarget)
wObj = ScatteringTransform.weightByDistance(wrappedTarget, pathCollection, St, distanceNorm, elementNorm)
weights = computeWeightMatrices(wrappedTarget, pathCollection, distanceNorm)


# fitting weighted
fitnessCallsHistoryW = Array{Int,1}()
fitnessHistoryW = Array{Float64,1}()
callbackW = function recW(oc)
    push!(fitnessCallsHistoryW, num_func_evals(oc))
    push!(fitnessHistoryW, best_fitness(oc))
end
setProbW = bbsetup(wObj; SearchRange=(-10., 10.), NumDimensions=128, PopulationSize=5000, MaxTime=30 * 60.0, CallbackFunction=callbackW, CallbackInterval=0.0, SaveTrace=true)
BlackBoxOptim.run!(setProbW)
for i = 1:2 * 12
    BlackBoxOptim.run!(setProbW)
end
save("weightedFunFit12Hr.jld2", "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW",fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals)
exit()
bc = best_candidate(setProbW.runcontrollers[end])
wObj(bc)
wObj(bc / 100) - wObj(bc)
using DSP
f = [1, 2, 3, 4, 3, 2, 1]; f = f ./ norm(f)
smoothed = DSP.conv(bc, f)[4:end - 3]; wObj(smoothed)
plot([bc, smoothed])
butFilt = filt(analogfilter(Lowpass(.4), Elliptic(4, 0.9, 30)), bc); plot([abs.(rfft(smoothed)), abs.(rfft(bc)), abs.(rfft(butFilt))], labels=["smooth" "bc" "filtered"])
(wObj(butFilt))
cosine(128)
plot(smoothed)
size(DSP.conv(bc, [1, 2, 1]))
plot(bc)
plot(abs.(rfft(bc)))
plotSecondLayer(St(bc))
plotSecondLayer(wrappedTarget)
plot([bc, data[:,:,1]])
plotSecondLayer(St(data[:,:,1]))
firstEx = St(data[:,:,1])
plot([firstEx[0] St(bc)[0]])
St(data[:,:,1]) .- St(bc)
