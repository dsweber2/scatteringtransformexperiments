using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using ScatteringTransform
using Test
using LinearAlgebra
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));

using GLMNet, Dates
FileIO.load("../glmnetCoefs.jld2")
JLD2.@load "glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
NTrain = 100
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
size(St.mainChain[1].fftPlan)
# fit postive and negative parts separately
Niter = 100; nExEach = 100; initGen = randn; inputSize = 128; trainThese = 1:size(β, 2)
βplus = max.(β, 0)
βminus = max.(-β, 0)
initPlus = initGen(inputSize, 1, nExEach, length(trainThese))
initPlus = reshape(data, (128, 1, 100, 3))
initMinus = initGen(inputSize, 1, nExEach, length(trainThese))
target = βplus[:,1]
obj = makeObjFun(target, St, true, 1e-0, true, false)

curX = initPlus[:,:,:,1]
curY = target
obj(curX,curY)
nAltern = 100; i = 0
singleRunTime = 60; singleRunIters = 2000

pathOfDescent, err, opt = fitReverseSt(singleRunIters, curX, obj=x -> obj(x, curY);
                                       errTol=1e10 * obj(curX, curY),
                                       timeLimit=Second(60 * singleRunTime));
obj
norm(curX)
sum((ScatteringTransform.flatten(St(curX)) .- curY).^2)
(ScatteringTransform.flatten(St(curX)) .- curY).^2
outX = ScatteringTransform.flatten(St(curX))
size(outX)
plot(mean(outX, dims=2))





StOne = scatteringTransform((size(data, 1), 1, 1), cw=dog2, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4)
StOne()
curX = data[:,:,1:1]
obj = makeObjFun(target, StOne, true, 1e-0, true, false)
obj(curX,curY)
singleRunIters = 1000;
pathOfDescentOne, errOne, optOne = fitReverseSt(singleRunIters, curX, obj=x -> obj(x, curY);
                                                errTol=1e10 * obj(curX, curY),
                                                timeLimit=Second(60 *
                                                                 singleRunTime),adjust=true);
curY = InterpolateInOutputDomain(curX, curY, target, StOne)
plot(curY)
StOne(pathOfDescentOne[:,:,:,end])
plot(ScatteringTransform.flatten(StOne(pathOfDescentOne[:,:,:,end])))
plot(minimum(abs.(pathOfDescentOne), dims=1:3)[1,1,1,:])
ii = findlast(minimum(abs.(pathOfDescentOne), dims=1:3)[1,1,1,:] .> 0)

plot(plot([curY, ScatteringTransform.flatten(StOne(pathOfDescentOne[:,:,:,ii]))],
     ylims=(0, 55), labels=["curY" "descend"]), plot(ScatteringTransform.flatten(StOne(pathOfDescentOne[:,:,:,1])), ylims=(0, 55)))
size(pathOfDescentOne)
plot(errOne)
singleRunIters = 90000
optOne = Momentum()

pathOfDescentOne, errOne, optOne = continueTrain(singleRunIters,
                                        pathOfDescentOne, errOne,
                                        opt=optOne, obj=x -> obj(x, curY),
                                        errTol=1e10 * obj(curX, curY),
                                        timeLimit=Second(60 * singleRunTime),
                                        adjust=true);
plot(errOne)
longTimeIters = 1000000; longTime = Hour(1)
pathOfDescentOne, errOne, optOne = continueTrain(longTimeIters,
                                        pathOfDescentOne, errOne,
                                        opt=optOne, obj=x -> obj(x, curY),
                                        errTol=1e10 * obj(curX, curY),
                                        timeLimit=longTime,
                                        adjust=true);
plot(errOne[1:ii])
