using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using ScatteringTransform
using Test
using LinearAlgebra
include("../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
singleRunIters = 1000;
StOne = scatteringTransform((size(data, 1), 1, 1), cw=dog2, 2, β=[2,2,1],
                            averagingLength=[2,3,4], outputPool=2 * 4)
target = ScatteringTransform.flatten(StOne(data[:,:,1:1]))[:,1]
obj = makeObjFun(target, StOne, true, 1e2, true, false)
obj(data[:,:,1:1],target)
initStart = randn(128,1,1);
norm(initStart)
obj(initStart,target)
pathOfDescentEx, errEx, optEx = fitReverseSt(1000, initStart, obj=x -> obj(x, target);
                                                opt=optEx,errTol=1e10 * obj(curX, curY),
                                                timeLimit=Minute(10),adjust=true);
plot(errEx)
xx = repeat(data[:,:,1]' * norm(pathOfDescentEx[:,1,1,1,end])/ norm(data[:,:,1]), 10)'
heatmap([pathOfDescentEx[:,1,1,:] xx])
size(pathOfDescent)
plot([pathOfDescent[:,1,1,1,end] xx[:,1]])
pathOfDescentEx, errEx, optEx = continueTrain(100000,
                                        pathOfDescentEx, errEx,
                                        opt=optEx, obj=x -> obj(x, target),
                                        errTol=1e10 * obj(curX, curY),
                                        timeLimit=Second(60 * singleRunTime),
                                        adjust=true);
