JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
ScatteringTransform.jointPlot(roll(β[:,1], St), "Cylinder",:viridis)
savefig("CylinderWrappedCoefs.pdf")
ScatteringTransform.jointPlot(roll(β[:,2], St), "Funnel",:viridis)
savefig("FunnelWrappedCoefs.pdf")
ScatteringTransform.jointPlot(roll(β[:,3], St), "Bell",:viridis)
savefig("BellWrappedCoefs.pdf")
targets = ["Cylinder","Funnel","Bell"]
targFile = ["Cyl", "Fun", "Bell"]
ii = 2
wrappedTarget = roll(βplus[:,ii], St)
pathCollection = nonZeroPaths(wrappedTarget)
wObj = ScatteringTransform.weightByDistance(wrappedTarget, pathCollection, St, distanceNorm, elementNorm)
obj(x) = sum((βplus[:,ii] .- ScatteringTransform.flatten(St(x))).^2)

targFile[ii]
spaceDom = load("weighted$(targFile[ii])Fit12Hr.jld2")
spacePop = spaceDom["individuals"]

plot(spaceDom["fitnessCallsHistoryW"], spaceDom["fitnessHistoryW"], title="Fitting positive $(targets[ii]) coefficients: descent",legend=:false)
spacePop = spaceDom["individuals"]
spaceObjScore = [wObj(x) for x in eachslice(spacePop, dims=2)]
spaceObjScoreUn = [obj(x) for x in eachslice(spacePop, dims=2)]
iSortSpace = sortperm(spaceObjScore)
iSortSpaceUn = sortperm(spaceObjScoreUn)
iSpace = argmin(spaceObjScore)
spaceSort = spacePop[:,iSortSpace]
spaceSortUn = spacePop[:,iSortSpaceUn]
bestSpace = spacePop[:,iSpace]

overTitle = scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="Best 20 Solutions, weighted. Target: $(targets[ii])", margin=1mm)
lay = @layout [a{.00001h}; grid(4, 5)]
plot(overTitle, [plot(x, legend=false, title="$(round(spaceObjScore[iSortSpace][ix], sigdigits=5))", titlefontsize=8, xticks=false) for (ix, x) in enumerate(eachcol(spaceSort[:,1:20]))]...,layout=lay)
savefig("best20Weighted$(targFile[ii]).pdf")
overTitle = scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="Best 20 Solutions, unweighted. Target: $(targets[ii])", margin=1mm)
lay = @layout [a{.00001h}; grid(4, 5)]
plot(overTitle, [plot(x, legend=false, title="$(round(spaceObjScoreUn[iSortSpaceUn][ix], sigdigits=5))", titlefontsize=8, xticks=false) for (ix, x) in enumerate(eachcol(spaceSortUn[:,1:20]))]...,layout=lay)
savefig("best20UnWeighted$(targFile[ii]).pdf")

heatmap(wrappedTarget[1])
plotSecondLayer(St(spaceSort[:,1]),title="Second Layer best example Cylinder")
savefig("secondLayer_$(targets[ii]).pdf")
plotSecondLayer(St(spaceSortUn[:,1]),title="Second Layer best example Cylinder unweighted")
savefig("secondLayer_unweight_$(targets[ii]).pdf")




plot([bestSpace,bestFourier * norm(bestSpace) / norm(bestFourier)],labels=["space" "fourier"],title="Best fit for positive cylinder weights")
plot(spacePop[:,rand(1:size(spacePop, 2), 100)],legend=false,title="random non-smooth selection")
plot(FourierPop[:,rand(1:size(spacePop, 2), 100)],legend=false,title="random non-smooth selection")
iFourier = argmin(FourierObjScore)
histogram([FourierObjScore, spaceObjScore], title="final population objective histogram",labels=["Fourier" "Space"])
f = [1, 2, 3, 4, 3, 2, 1]; f = f ./ norm(f)
smoothed = DSP.conv(f, bestFourier)[4:end - 3]; wObj(bestFourier) - wObj(smoothed)
smoother(x) = DSP.conv(f, x)[4:end - 3]
smoothFourier = DSP.conv(f, FourierPop)[4:end - 3,:]
wObj(smoother(bestFourier))
FourierObjScoreSmooth = [wObj(x) for x in eachslice(smoothFourier, dims=2)]
smoother(FourierPop)
smoother(bestFourier)
plot(smoother(bestFourier),)
plotSecondLayer(St(smoother(bestFourier)),title="best Fourier")
plotSecondLayer(St(smoother(bestFourier)),title="best Fourier")
plotSecondLayer(St(bestSpace),title="best Fourier")
plotSecondLayer(wrappedTarget)















pwd()
ii = 2
FourierDom = load("weighted$(targFile[ii])FourierFit12Hr.jld2")
x̂ = FourierDom["individuals"]
FourierPop = irfft(x̂[1:65,:] .* exp.(im .* x̂[66:end,:]), 128, 1)

plot(FourierDom["fitnessCallsHistoryW"], FourierDom["fitnessHistoryW"], title="Fitting positive $(targets[ii]) coefficients: descent",legend=:false)
plot!(spaceDom["fitnessCallsHistoryW"], spaceDom["fitnessHistoryW"], title="Fitting positive $(targets[ii]) coefficients: descent",legend=:false)
spacePop = spaceDom["individuals"]
spaceObjScore = [wObj(x) for x in eachslice(spacePop, dims=2)]
spaceObjScoreUn = [obj(x) for x in eachslice(spacePop, dims=2)]
iSortSpace = sortperm(spaceObjScore)
iSortSpaceUn = sortperm(spaceObjScoreUn)
iSpace = argmin(spaceObjScore)
spaceSort = spacePop[:,iSortSpace]
spaceSortUn = spacePop[:,iSortSpaceUn]
bestSpace = spacePop[:,iSpace]

function plotError(x, y, dims=2;kwargs...)
    meany = mean(y, dims=dims)
    stdy = std(y, dims=dims)
    plot(x, meany .+ stdy, fill=meany .- stdy, label="std")
    plot!(x, meany; kwargs...)
end
plotError(1:128, spacePop;title="Variation over space trained population in space")
plotError(1:65, abs.(rfft(spacePop, 1));title="Variation over space trained population in frequency")

plot(plotError(1:128, spacePop;title="Variation over space trained population in space"),
     plotError(1:128, FourierPop;title="Variation over Frequency trained population in space"),layout=(2, 1))
plot(plotError(1:65, abs.(rfft(spacePop, 1));title="Variation over space trained population in frequency"),
     plotError(1:65, abs.(rfft(FourierPop, 1));title="Variation over Frequency trained population in frequency"),layout=(2, 1))
savefig("varationInPopulationFourier.pdf")
overTitle = scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="Best 20 Solutions, weighted. Target: $(targets[ii])", margin=1mm)
lay = @layout [a{.00001h}; grid(4, 5)]
plot(overTitle, [plot(x, legend=false, title="$(round(spaceObjScore[iSortSpace][ix], sigdigits=5))", titlefontsize=8, xticks=false) for (ix, x) in enumerate(eachcol(spaceSort[:,1:20]))]...,layout=lay)
savefig("best20Weighted$(targFile[ii]).pdf")
overTitle = scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="Best 20 Solutions, unweighted. Target: $(targets[ii])", margin=1mm)
lay = @layout [a{.00001h}; grid(4, 5)]
plot(overTitle, [plot(x, legend=false, title="$(round(spaceObjScoreUn[iSortSpaceUn][ix], sigdigits=5))", titlefontsize=8, xticks=false) for (ix, x) in enumerate(eachcol(spaceSortUn[:,1:20]))]...,layout=lay)
savefig("best20UnWeighted$(targFile[ii]).pdf")














plotError(1:128, spacePop;title="Population Variation")
savefig("population_variation.png")
