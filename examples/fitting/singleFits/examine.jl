using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra,Statistics
using CUDA, Zygote, Flux, FFTW, DSP
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../../simpleSignalExamples.jl")
N = 100; NTrain = 1
f = 5; s = 7; l = 4
p = pathLocs(2, (l, s, f))
cd("singleFits")
d = load("weighted12Hr_DCT_f$(f)s$(s)l$(l).jld2")
d2 = load("weighted12Hr_f$(f)s$(s)l$(l).jld2")
plot(d["fitnessCallsHistoryW"], d["fitnessHistoryW"], xlabel="number of function calls", ylabel="weighted error",title="Fitting descent ($f,$s), location $l",legend=false)
plot!(d2["fitnessCallsHistoryW"], d2["fitnessHistoryW"], xlabel="number of function calls", ylabel="weighted error", title="Fitting descent ($f,$s), location $l",legend=false)
savefig("figures/fittingDescent_f$(f)s$(s)l$(l).png")
St = scatteringTransform((128, 1, 1), cw=dog2, 2, β=[2,2,1], averagingLength=[2,3,4], outputPool=2 * 4)
null = St(zeros(128))
null.output[3][l,s,f,1] = 1f0
nullTarget = ScatteringTransform.normalize(null, 1)
distanceNorm = 1.0; elementNorm = 2.0
norm(nullTarget - St(randn(128)))
nullTarget[p] - St(randn(128))[p]
justTarg(x) = nullTarget[p] - St(x)[p] # if we only care about the actual target location
obj(x) = norm(nullTarget - St(x)) # unweighted objective
wObj = ScatteringTransform.weightByDistance(nullTarget, (p,), St, distanceNorm, elementNorm)
P = plan_dct(zeros(128))
function FourierObj(x̂)
    x = P \ x̂
    wObj(x)
end
FourierObj(zeros(128))
spacePop = d["individuals"]
scores = [FourierObj(x) for x in eachslice(spacePop, dims=2)]
scores = [wObj(x) for x in eachslice(spacePop, dims=2)]
unweightedScores = [obj(x) for x in eachslice(spacePop, dims=2)]
scoresUn = unweightedScores
scoresPath = [justTarg(x) for x in eachslice(spacePop, dims=2)]
iSort = sortperm(scores)
iSortUn = sortperm(unweightedScores)
iSortPath = sortperm(scoresPath)
histogram(unweightedScores)
histogram(scores)
sorted = spacePop[:,iSort]
sortedUn = spacePop[:, iSortUn]
sortedPath = spacePop[:,iSortPath]
weights = ScatteringTransform.computeWeightMatrices(nullTarget, (p,), 1.0)
heatmap(weights[3][:,:,1], xlabel="first layer index", ylabel="second layer index", title="penalizing distant paths; current path is $f,$s, at location $l")
savefig("exampleDistancePenalty.pdf")

plot(spacePop[:,rand(1:size(spacePop, 2), 10)],legend=false)

function plotFirstXEx(sorted, scores,  title="Over title", apFun=identity, gridSizes=(4, 5))
    overTitle = scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title=title)
    lay = @layout [a{.00001h}; grid(gridSizes...)]
    plot(overTitle, [plot(apFun(x), legend=false, title="$(round(scores[ix], sigdigits=5))", titlefontsize=8, xticks=false) for (ix, x) in enumerate(eachcol(sorted[:,1:prod(gridSizes)]))]..., layout=lay)
end
# top 20 examples, sorted by weighted objective
plotFirstXEx(sorted,scores[iSort],"Best 20 Solutions, weighted. Target: path ($(f),$(s)), location $(l)")
savefig("figures/examples_f$(f)s$(s)l$(l).pdf")
# Fourier domain plot of the same
plotFirstXEx(sorted,scores[iSort],"Best 20 Solutions, weighted, frequency domain.\n Target: path ($(f),$(s)), location $(l) (blue real, orange imaginary)",x -> [real.(rfft(x)) imag.(rfft(x))])
savefig("figures/examples_fourier_f$(f)s$(s)l$(l).pdf")

# top 20 by unweighted objective
plotFirstXEx(sortedUn, scoresUn[iSortUn],"Best 20 Solutions by total L2 norm. Target: path ($(f),$(s)), location $(l)")
savefig("figures/examples_by_overall_f$(f)s$(s)l$(l).pdf")
# Fourier domain plot of the same
plotFirstXEx(sortedUn, scoresUn[iSortUn],"Best 20 Solutions by total L2 norm, frequency domain.\n Target: path ($(f),$(s)), location $(l) (blue real, orange imaginary)",x -> [real.(rfft(x)) imag.(rfft(x))])
savefig("figures/examples_by_overall_fourier_f$(f)s$(s)l$(l).pdf")

plotFirstXEx(sortedPath, scores[iSortPath],"Best 20 Solutions by target location. Target: path ($(f),$(s)), location $(l)")
savefig("figures/examples_by_loc_f$(f)s$(s)l$(l).pdf")
plotFirstXEx(sortedPath, scores[iSortPath],"Best 20 Solutions by target location, frequency domain.\n Target: path ($(f),$(s)), location $(l) (blue real, orange imaginary)", x -> [real.(rfft(x)) imag.(rfft(x))])
savefig("figures/examples_by_loc_fourier_f$(f)s$(s)l$(l).pdf")
function smoother(x, i=2)
    f = [1:i... (i - 1):-1:1...]'
    f = f ./ norm(f)
    DSP.conv(x, f)[i:(end - i + 1)]
end
tmp = randn(128); plot([tmp smoother(tmp, 4)])
wObj(sorted[:,1]) .- [wObj(smoother(sorted[:,1], i)) for i = 1:5] # if any of these are positive, then smoothing reduces the error
plot([sorted[:,1] smoother(sorted[:,1], 4)])
heatmap(St(sorted[:,1])[1][:,:,1]')
heatmap(St(sortedUn[:,1])[1][:,:,1]')
heatmap(St(sortedPath[:,3])[1][:,:,1]')
plotSecondLayer(St(sorted[:,1]),title="Second Layer fitting path $f, $s at $l")
savefig("figures/secondlayer_f$(f)s$(s)l$(l).pdf")
plotSecondLayer(St(sortedUn[:,1]),title="Second Layer fitting path $f, $s at $l, best unweighted")
savefig("figures/secondlayer_unweighted_f$(f)s$(s)l$(l).pdf")
plotSecondLayer(St(sortedPath[:,1]),title="Second Layer fitting path $f, $s at $l, best unweighted")
savefig("figures/secondlayer_pathOnly_f$(f)s$(s)l$(l).pdf")
plotSecondLayer(St(sorted[:,10]),title="Second Layer fitting path $f, $s at $l")
savefig("figures/secondlayer_f$(f)s$(s)l$(l)_ex10.pdf")

plot(scores[iSort]) # the rate of decay in the loss
# try averaging the best
N = 300
meanOfTheScores = [ mean(scores[iSort][1:i]) for i = 1:N]
scoreOfTheMean = [wObj(mean(sorted[:, 1:i], dims=2)) for i = 1:N]
plot([scoreOfTheMean meanOfTheScores],labels=["score of the mean" "mean of the scores"])
betterMeanI = argmin(scoreOfTheMean)
betterCandid = sorted[:,betterMeanI]
wObj(betterCandid)
plot([sorted[:,1],betterCandid], title="Comparing averaging with the single best",labels=["single best" "average"])

plot([real.(rfft(betterCandid)), imag.(rfft(betterCandid))])

# does shifting by hand get a better result?
mirrored = [betterCandid; reverse(betterCandid)]
mirroredUn = [sortedUn[:,1]; reverse(sortedUn[:,1])]
p1 = plot(mirrored, title="Mirrored Best example weighted", legend=false)
plot([mirrored circshift(mirrored, 52) -circshift(mirrored, 26)],labels=["original" "shifted by 52" "flipped and shifted by 26"])
savefig("figures/mirroredExampleWeight$(f)$(s)$(l).pdf")
p2 = plot(mirroredUn, title="Mirrored Best example unweighted", legend=false)
savefig("figures/mirroredExampleUn$(f)$(s)$(l).pdf")
plot(p1,p2,layout=(2, 1))
savefig("figures/mirroredExample$(f)$(s)$(l).pdf")
plot([mirrored mirroredUn], title="Mirrored Best examples", legend=:top, labels=["weighted" "unweighted"])
savefig("figures/mirroredExample$(f)$(s)$(l).pdf")
shiftScores = [wObj(circshift(mirrored, i)[1:128]) for i = 0:128]
plot(wObj(betterCandid) .- shiftScores)
# no
plotScatteringTransform(sctRes,title="", )

# does doing gradient descent afterwards get a better result?
# not by much really.
singleRunTime = 1; singleRunIters = 2000
# optim method
res, λ = fitUsingOptim(100, betterCandid, p, nullTarget, St, allowIncrease=true, obj=wObj)
plot([res.minimizer, betterCandid], labels=["also grad descent" "average of best 10 from black box"])
res.minimum
errThisRun = [x.value for x in res.trace]
plot(errThisRun)
# momentum methods
pathOfDescent, err, opt = fitReverseSt(singleRunIters, betterCandid, obj=wObj; opt=Momentum(1e-9), errTol=1e10 * wObj(betterCandid), timeLimit=Second(60 * singleRunTime));
plot!(err);
plot!(scores[iSort][1:200])
size(pathOfDescent)
plot(pathOfDescent[:,1,1,1:100:end],legend=false)


# does doing gradient descent afterwards get a better result? using the actual objective
#
bestUnweighted = sortedUn[:,1]
singleRunTime = 1; singleRunIters = 2000
# optim method
res, λ = fitUsingOptim(100, bestUnweighted, p, nullTarget, St, allowIncrease=false, obj=obj)
errThisRun = [x.value for x in res.trace]
plot(errThisRun)
plot([res.minimizer, bestUnweighted], labels=["also grad descent" "average of best 10 from black box"])
using Dates
pathOfDescent, err, opt = fitReverseSt(2000, randn(128), obj=wObj; opt=Momentum(1e-9), errTol=1e10 * wObj(randn(128)), timeLimit=Second(60 * 5));






function jointPlot(thingToPlot, thingName, cSymbol)
    thingToPlot = rollβ[:,i]
    clims = (min(minimum.(thingToPlot)...), max(maximum.(thingToPlot)...))
    toHeat = sum(thingToPlot[2], dims=1)[1,:,:]
    firstLay = thingToPlot[1]'
    toHeat[toHeat .== 0] .= -Inf    # we would like zeroes to not actually render
    firstLay[firstLay .== 0] .= -Inf # for either layer

    zeroAt = -clims[1] / (clims[2] - clims[1]) # set the mid color switch to zero
    c = cgrad(cSymbol, [0,zeroAt])
    p1 = plotSecondLayer(thingToPlot, title="$(thingName) Second Layer", toHeat=toHeat, logPower=false, c=c, clims=clims, subClims=clims, cbar=false, yticks=1:2:27, xticks=1:2:32, xVals=(.000, .993), yVals=(0.0, 0.994))
    p2 = heatmap(firstLay, c=c, title="$(thingName) First Layer", xlabel="location", ylabel="frequency", clims=clims, cbar=false)
    colorbarOnly = scatter([0,0], [0,1], zcolor=[0,3], clims=clims, xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", c=c, grid=false, framestyle=:none)
    lay = @layout [grid(1, 2) a{.04w}]
    plot(p1, p2, colorbarOnly, layout=lay)
end

# bandpass
responseType = Bandpass(.000001, 250; fs=1000)
designMethod = Butterworth(4)
responseType = Lowpass(50; fs=1000)
designMethod = Chebyshev2(2, 1)
filtered = filt(digitalfilter(responseType, designMethod), mirrored)
plot(abs.([rfft(mirrored) rfft(filtered)]))
plot([mirrored filtered],legend=false,title="DSP filtering on (5,5) (orange is filtered)")
savefig("figures/dsp_mirror_filter.png")
function hardBandpass(x, ncut)
    unmirSize = floor(Int, length(x) / 2)
    cutoff = [ones(ncut); zeros(unmirSize + 1 - ncut)]
    irfft(rfft(x) .* cutoff, length(x))[1:unmirSize]
end
function findBestHardBandpass(x, obj, bandpass=hardBandpass)
    mirrored = cat(x, reverse(x, dims=1), dims=1)
    evalFilters = [obj(bandpass(mirrored, ncut)) for ncut = 2:size(x, 1)]
    cutFreq = argmin(evalFilters)
    return bandpass(mirrored, cutFreq + 1), evalFilters[cutFreq], cutFreq + 1
end
besterCandid, wObjVal, cutFreqW = findBestHardBandpass(sorted[:,1], wObj)
objCandid, objVal, cutFreqUn = findBestHardBandpass(sortedUn[:,1], obj)
cutFreq, cutFreqUn
p1 = plot([sorted[:,1] besterCandid], labels=round.([wObj(sorted[:,1]) wObjVal], sigdigits=5), title="Hard Bandpass optimal cutoff\nWeighted", legend=:outerright)
p2 = plot([sortedUn[:,1] objCandid], labels=round.([obj(sortedUn[:,1]) objVal], sigdigits=5), title="Unweighted", legend=:outerright)
plot(p1,p2,layout=(2, 1))
savefig("figures/hardBandpass_mirror_filter.png")
plot(abs.(rfft(besterCandid)))
findBestHardBandpass(bestUnweighted,obj)
plot(bestUnweighted)
ncut = round(Int, 128 * 2 / 4); cutoff = [ones(ncut); zeros(129 - ncut)]
tmpO = obj(mirrored[1:128])
evalFilters = [obj(hardBandpass(mirrored, ncut)) for ncut = 2:128]
cutoffFreq = argmin(evalFilters)
plot([obj(hardBandpass(mirrored, ncut)) for ncut = 2:128])
hline!([tmpO tmpO])
plot([mirrored[1:128] hardBandpass(mirrored, cutoffFreq) findBestHardBandpass(bestUnweighted, obj)])
hardBandpassed = irfft(rfft(mirrored) .* cutoff, 256)
wObj(mirrored[1:128])
wObj(hardBandpassed[1:128])
plot([mirrored irfft(rfft(mirrored) .* cutoff, 256)],legend=false)
plot(abs.(rfft(mirrored) .* cutoff))

# lowpass
responseType = Lowpass(250; fs=1000)
designMethod = Butterworth(1)
filtered = filt(digitalfilter(responseType, designMethod), bestUnweighted)
plot(abs.([rfft(bestUnweighted) rfft(filtered)]))
plot([bestUnweighted filtered])

FourierObj(passSorted[:,1])
passSorted[:,1]
# what happens if we lowpass the whole population?
plot(sorted[:,1])
function BandpassStayInFourier(x)
    flattenFrom(x, i) = cat(x[1:i,axes(x)[2:end]...], zeros(size(x, 1) - i), dims=1)
    evalFilters = [FourierObj(flattenFrom(x, ncut)) for ncut = 2:size(x, 1)]
    cutFreq = argmin(evalFilters)
    return flattenFrom(x, cutFreq + 1), evalFilters[cutFreq], cutFreq + 1
end
flattened, value, ind = BandpassStayInFourier(sorted[:,1])
allPassed = cat(sorted[1:ind,:], zeros(size(sorted, 1) - ind, size(sorted, 2)), dims=1)
passedScores = [FourierObj(x) for x in eachslice(allPassed, dims=2)]
histogram(passedScores)
histogram!(scores)
iSortPass = sortperm(passedScores)
passSorted = allPassed[:, iSortPass]
plot(idct(passSorted[:,1]))
findBestHardBandpass(sorted[:,1],FourierObj,flattenFrom)
objVals = [FourierObj(flattenFrom(sorted[:,1], i)) for i = 2:128]
plot(objVals)
best, score, ind = findBestHardBandpass(sorted[:,1], FourierObj)
plot(best)
spacePop


function plotError(x, y, dims=2;kwargs...)
    meany = mean(y, dims=dims)
    stdy = std(y, dims=dims)
    plot(x, meany .+ stdy, fill=meany .- stdy, label="std")
    plot!(x, meany; kwargs...)
end
plotError(1:128, spacePop;title="Variation over population in dct freq")
plotError(1:128, freqPop;title="Variation over population in dct freq")

plotError(1:128, idct(spacePop, 1);title="Variation over population in space")

# what are the frequencies?
using DSP
size(St.mainChain[1].fftPlan,1)
function getMeanFrequencies(St, fs=1000)
    l1 = St.mainChain[1]
    l2 = St.mainChain[4]
    s1, s2 = size(l1.fftPlan, 1), size(l2.fftPlan, 1)
    downSample = s2 / s1
    weights1 = ifftshift(irfft(l1.weight, s1, 1), 1)
    ω1 = [meanfreq(weights1[:,i], fs) for i = 1:size(weights1, 2)]
    weights2 = ifftshift(irfft(l2.weight, s2, 1), 1)
    ω2 = [meanfreq(weights2[:,i], fs * downSample) for i = 1:size(weights2, 2)]
    weights2 = ifftshift(irfft(St.mainChain[4].weight, 171, 1), 1)
    return ω1, ω2
end
ω1, ω2 = getMeanFrequencies(St)










# tmp
rfft(randn(128))
tmpSig = im .* zeros(65); tmpSig[3] = 1 + 0im; tmpSig[2] = 1 + 0im
mirrorTmp = [irfft(tmpSig, 128); reverse(irfft(tmpSig, 128))]
plot(dct(mirrorTmp))
plot(plot([real.(rfft(mirrorTmp)) imag.(rfft(mirrorTmp))], legend=false, title="mirrored"), plot([real.(tmpSig) imag.(tmpSig)], legend=false, title="original"),layout=(2, 1))
plot(mirrorTmp)
tmpF = irfft(randn(128))
mirrored = [betterCandid; reverse(betterCandid)]
mirrored = [bestUnweighted; reverse(bestUnweighted)]
tmpSig = zeros(128); tmpSig[4] = -1.0
plot(idct(tmpSig))
