
using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "weightedCylFit12Hr.jld2"
BlackBoxOptim.run!(setProbW)
for i = 1:2 * 12
    BlackBoxOptim.run!(setProbW)
end
save("weightedCylFit12Hr.jld2", "setProbW",setProbW, "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW",fitnessHistoryW,)
