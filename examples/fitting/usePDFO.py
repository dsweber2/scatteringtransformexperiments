#!/usr/bin/env python3

from pdfo import pdfo
from matplotlib import pyplot as plt
import pdfo as wholeModule
import numpy as np

from julia.api import Julia

jl = Julia(compiled_modules=False)
from julia import Base
from julia import ScatteringTransform
from julia import ContinuousWavelets as CW
from julia import JLD2
from julia import FileIO
from julia import Main
from julia import GLMNet

Main.include("../../simpleSignalExamples.jl")
data = Main.generateData(100)
data = data.reshape((128, 1, 300))
St = ScatteringTransform.scatteringTransform(
    data[:, :, 0:1].shape,
    2,
    cw=CW.dog2,
    β=[2, 2, 1],
    averagingLength=[2, 3, 4],
    outputPool=2 * 4,
)
data[:, :, 0:1].shape
sing = St(data[:, :, 0:1])
print(Main.size(data))
print(sing)

glm_path_cv = FileIO.load("../glmnetCoefs.jld2")
glm_path_cv = glm_path_cv["glmPathCV"]
betas = glm_path_cv.path.betas
i_lambda = GLMNet.chooseBestLambda(glm_path_cv)
init_ex = np.random.randn(128, 1, 1)
target = np.maximum(betas[:, :, i_lambda], 0)
ScatteringTransform.makeObjFun
obj = ScatteringTransform.makeObjFun(target[:, 1], St, True, 1e-0)
obj(init_ex)
options = {"quiet": False, "rhoend": 1e-10, "rhobeg": 10}
pdf_res = pdfo(obj, init_ex, options=options)
Main.size(St)

# see if we can recreate an example exactly, since we know that the error there
# is zero
target2 = ScatteringTransform.flatten(St(data[:, :, 0:1]))
obj2 = ScatteringTransform.makeNormMatchObjFun(
    target2[:, 0], St, np.linalg.norm(data[:, :, 0:1]), 1e-0
)
init_ex2 = np.random.randn(128, 1, 1)
print(obj2(init_ex2))
print(obj2(data[:, :, 0:1]))
options2 = {"quiet": False, "rhoend": 1e-7, "rhobeg": 10}
pdf_res_known = pdfo(obj2, init_ex2, options=options2)
firstFit = pdf_res_known
plt.plot(pdf_res_known.fhist)
plt.yscale("log")
plt.show()
pdf_res_known.x
plt.plot(pdf_res_known.x)
plt.show()
