using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using CUDA, Zygote, Flux, FFTW
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using GLMNet
include("../../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
JLD2.@load "../glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
βplus = max.(β, 0)
βminus = max.(-β, 0)
wrappedTarget = roll(βplus[:,2], St)
saveName = "weightedFunDCTFit12Hr.jld2"
wrappedTarget = ScatteringTransform.normalize(wrappedTarget, 1)
distanceNorm = 1.0; elementNorm = 2.0
pathCollection = nonZeroPaths(wrappedTarget)
wObj = ScatteringTransform.weightByDistance(wrappedTarget, pathCollection, St, distanceNorm, elementNorm)
P = plan_dct(zeros(128))
function FourierObj(x̂)
    x = P \ x̂
    wObj(x)
end

# fitting weighted
fitnessCallsHistoryW = Array{Int,1}()
fitnessHistoryW = Array{Float64,1}()
callbackW = function recW(oc)
    push!(fitnessCallsHistoryW, num_func_evals(oc))
    push!(fitnessHistoryW, best_fitness(oc))
end
setProbW = bbsetup(FourierObj; SearchRange=(-100.0, 100.0), NumDimensions=128, PopulationSize=5000, MaxTime=30 * 60.0, CallbackFunction=callbackW, CallbackInterval=0.0, SaveTrace=true)
BlackBoxOptim.run!(setProbW)
for i = (1:2 * 12) .- 1
    BlackBoxOptim.run!(setProbW)
    save(saveName, "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW", fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals)
end
save(saveName, "fitnessCallsHistoryW", fitnessCallsHistoryW, "fitnessHistoryW",fitnessHistoryW, "individuals", setProbW.runcontrollers[end].optimizer.population.individuals)
exit()



JLD2.@load "../weightedCylFourierFit12Hr.jld2"
spaceDom = load("weightedCylFit12Hr.jld2")
plot(fitnessCallsHistoryW, fitnessHistoryW,label="Fourier Domain")
plot!(spaceDom["fitnessCallsHistoryW"], spaceDom["fitnessHistoryW"], label="space domain", title="Fitting positive cylinder coefficients: descent")
savefig("compareFourierRepNormalRep.pdf")
spacePop = spaceDom["individuals"]
FourierPop = irfft(individuals[1:65,:] + im * individuals[66:end,:], 128, 1)
spaceObjScore = [wObj(x) for x in eachslice(spacePop, dims=2)]
FourierObjScore = [FourierObj(x) for x in eachslice(individuals, dims=2)]
iSpace = argmin(spaceObjScore)
iFourier = argmin(FourierObjScore)
bestSpace = spacePop[:,iSpace]
bestFourier = FourierPop[:,iFourier]
plot([bestSpace,bestFourier * norm(bestSpace) / norm(bestFourier)],labels=["space" "fourier"],title="Best fit for positive cylinder weights")
plot(spacePop[:,rand(1:size(spacePop, 2), 100)],legend=false,title="random non-smooth selection")
plot(FourierPop[:,rand(1:size(spacePop, 2), 100)],legend=false,title="random non-smooth selection")
iFourier = argmin(FourierObjScore)
histogram([FourierObjScore, spaceObjScore], title="final population objective histogram",labels=["Fourier" "Space"])
f = [1, 2, 3, 4, 3, 2, 1]; f = f ./ norm(f)
smoothed = DSP.conv(f, bestFourier)[4:end - 3]; wObj(bestFourier) - wObj(smoothed)
smoother(x) = DSP.conv(f, x)[4:end - 3]
smoothFourier = DSP.conv(f, FourierPop)[4:end - 3,:]
wObj(smoother(bestFourier))
FourierObjScoreSmooth = [wObj(x) for x in eachslice(smoothFourier, dims=2)]
smoother(FourierPop)
smoother(bestFourier)
plot(smoother(bestFourier),)
plotSecondLayer(St(smoother(bestFourier)),title="best Fourier")
plotSecondLayer(St(smoother(bestFourier)),title="best Fourier")
plotSecondLayer(St(bestSpace),title="best Fourier")
plotSecondLayer(wrappedTarget)
smoothBestSpace =
smoothedSpace =


fnto(x) = fieldnames(typeof(x))
fnto(setProbW)
fnto(setProbW.runcontrollers[1])
fnto(setProbW.runcontrollers[1].optimizer)
setProbW.runcontrollers[1].optimizer.name
setProbW.runcontrollers[1].optimizer.select
fnto(setProbW.runcontrollers[1].optimizer)
fnto(setProbW.runcontrollers[1].optimizer.population.candi_pool[1])
fnto(setProbW.runcontrollers[1].optimizer.population.candi_pool[1])
setProbW.runcontrollers[1].optimizer.population.candi_pool[1]
fnto(setProbW.runcontrollers[1].optimizer.population)
# modify appears to just be the settings for F and Cr
setProbW.runcontrollers[1].optimizer.modify.params.fdistr
sample
