using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using ScatteringTransform
using Test
using LinearAlgebra
include("../simpleSignalExamples.jl")
N = 100; NTrain = 1
y = ones(3 * N); y[1 + N:2N] *= 2; y[2N + 1:3N] *= 3
data = reshape(generateData(100), (128, 1, 300));
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
St = St |> gpu
# fit each of the 3 locations used in the hyper sparse case
using Random
JLD2.@load "trainedl1Models.jld2"
JLD2.@load "sparseLog2.jld2"
sparsest = roll(fitted_params(lambda1000).best_fitted_params.coefs, St)
sparsest = roll(fitted_params(selfTunerSparser).best_fitted_params.coefs, St)
secondLayerLocs = findall(abs.(sparsest[2]) .> 1e-6)
length(secondLayerLocs)
secondLayerLocs
p = pathLocs(2, Tuple(secondLayerLocs[3])[1:end - 1]) # corresponds to second class
obj = makeObjFun(sparsest[:,2], p, St)
N = 1000
opt = Momentum(1)
initGuess = randn(Float32, 128, 1, NTrain);
@time obj(initGuess)
@time gradient(obj, initGuess);

pathOfDescent, errr = fitReverseSt(N, initGuess, opt=opt, obj=obj,
                                   ifGpu=identity);
sparsest[:,2]
paths, err = maximizeSingleCoordinate(500, initGuess, p, sparsest[:,2], St,
                                      obj=nothing, tryCatch=false)
size(paths[end].minimizer)
plot(paths[end].minimizer[:,1,1])
plotSecondLayer(St(paths[end].minimizer))
plot(cat(err..., dims=1),yscale=:log10)
plot(err,yscale=:log10)
opt = Momentum(.01)
pathOfDescent, err = continueTrain(N, pathOfDescent, errr, opt=opt, obj=obj);
size(pathOfDescent)
plotSecondLayer(St(pathOfDescent[:,:,:,2001])[:,1])
plot(pathOfDescent[:,1,1,2001])
plot(pathOfDescent[:,1,3,2001])
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init,l)
save("fitting_firstPath.jld2", "mean1path",pathOfDescent,"mean1err",err)
JLD2.@load "fitting_firstPath.jld2"
plot(cat(mean1err..., dims=1), yscale=:log10, legend=false)
size(mean1path)
plot(mean1path[:,1,1,end])
plot(mean1path[:,1,6,end])
heatmap(mean1path[:,1,:,end]', xlabel="space", ylabel="examples")

# fit the whole LDA solution
JLD2.@load "lda_solution.jld2"
NTrain = 100
ldaMeans = roll(fitted_params(ldaFit).projected_class_means, St)
target = ldaMeans[:,1]
plotSecondLayer(target)
obj = makeObjFun(target, St)
init = randn(Float32, 128, 1, NTrain)
obj(randn(size(init)))
obj(mean1path[:,:,:,end])
N = 1000
opt = Momentum(1)
pathOfDescent, errr = fitReverseSt(N, init, opt=opt, obj=obj);
opt = Momentum(10)
opt = Momentum(.01)
pathOfDescent, errr = continueTrain(500, pathOfDescent, errr, opt=opt, obj=obj);
pertRes, λ, errGrouped, totalUsed = fitByPerturbing(10,pathOfDescent[:,:,:,end], pathLocs(0, :, 1, :, 2, :),
                                                    target, St)
heatmap(pertRes.minimizer[:,1,:]')
plot(pertRes.minimizer[:,1,6])
errGrouped
plot(errr, yscale=:log10, title="fitting lda Bell mean error (100 examples)")
plotSecondLayer(St(pathOfDescent[:,:,:,end])[:,1], title="lda fit")
heatmap(pathOfDescent[:,1,:,end]', xlabel="space", ylabel="examples")
heatmap(pathOfDescent[:,1,1,:]',ylabel="descent")
plot(pathOfDescent[:,1,3,end])
save("fitting_lda.jld2", "pertRes", pertRes, "λ",λ, "errGrouped", errGrouped, "totalUsed",totalUsed)
JLD2.@load "fitting_lda.jld2"
errr[end] / errr[1]
plot(errr, yscale=:log10)
l = @layout [a; [b; c] d]
plotDescentResults(pathOfDescent, errr, N, init, l)


using GLMNet
JLD2.@load "glmnetCoefs.jld2"
iλ = chooseBestLambda(glmPathCV)
β = glmPathCV.path.betas[:,:,iλ]
a0 = glmPathCV.path.a0[:,iλ]
NTrain = 99
St = scatteringTransform((size(data, 1), 1, NTrain), cw=dog2, 2, β=[2,2,1],
                         averagingLength=[2,3,4], outputPool=2 * 4)
fullβ = roll(β, St)
target = fullβ[:,1]
obj = makeObjFun(target, St)
init = randn(Float32, 128, 1, NTrain)
obj(randn(size(init)))
obj(mean1path[:,:,:,end])
N = 1000
pertRes, λ, errGrouped, totalUsed = fitByPerturbing(10,init, pathLocs(0, :, 1, :, 2, :),
                                                    target, St)
save("fitting_glmnet.jld2", "pertRes", pertRes, "λ", λ, "errGrouped", errGrouped, "totalUsed", totalUsed)
size(β)
size(ScatteringTransform.flatten(St(init)))
size(a0)


# using this as a generative model
nEach = Int(NTrain / 3)
targs = cat(repeat([1 0 0], nEach)', repeat([0 1 0], nEach)', repeat([0 0 1], nEach)', dims=2)
a0 .+ β' * ScatteringTransform.flatten(St(init))
softmax(a0 .+ β' * ScatteringTransform.flatten(St(init)))
# multinomial regression from GLMNet
function pred(x, st, β, a0)
    base = β' * ScatteringTransform.flatten(st(x))
    softmax(softmax(a0 .+ base))
end
μ = size(x)[end] * 1e-5
norm(targs - pred(init, St, β, a0))
obj(x) = norm(targs - pred(x, St, β, a0)) + μ * norm(x)
obj(init)
res = fitByPerturbing(100, init, pathLocs, targs, St; obj=obj,λ=1.0)
y, ∂ = pullback(obj, (init))
tmp = ∂(y)[1]
typeof(tmp)
size(tmp)
plot(res[end - 1])
training = res[1]
heatmap(training.minimizer[:,1,:])



curY' * ScatteringTransform.flatten(St(curX))


# fit postive and negative parts separately
Niter = 100; nExEach = 99; initGen = randn; inputSize = 128; trainThese = 1:size(β, 2)
βplus = max.(β, 0)
βminus = max.(-β, 0)
initPlus = initGen(inputSize, 1, nExEach, length(trainThese))
initMinus = initGen(inputSize, 1, nExEach, length(trainThese))
β
minimum(βplus) == 0
minimum(βminus) == 0
size(β)
size(initPlus)
initPlus[:,:,:,1]

target = βplus[:,1]
# adjustTarget()
jointObj = 3
obj = makeObjFun(target, St, true, 1e-0, true, false)
obj(curX, curY)
-sum(curY' * ScatteringTransform.flatten(St(curX))) + norm(curX)

# why is the step size zero??!?!?
y, ∂ = pullback(x -> obj(x, target), curX)
obj(curX,target)
evaling = [obj(curX + α * ∂(y)[1], target) for α = exp.(range(-5, 3, length=10))]
exp.(range(-5, 3, length=10))
evaling
curY = target
y, ∂ = pullback(x -> obj(x, curY), optX.minimizer)
size(∂(y)[1])
obj(curX,target)
αr = exp.(range(-8, 0, length=10))
evaling = [obj(optX.minimizer + α * ∂(y)[1], target) for α = αr]
plot(αr, evaling, xscale=:log10, yscale=:log10); hline!([y])
plot(diff(pathOfYs, dims=2),legend=:bottomright)
plot(curX[:,1,7])
initPathOfYs = pathOfYs
# up to here is temp experiments
curX = initPlus[:,:,:,1]
curY = target
nAltern = 100; i = 0
lineSearch = BackTracking()
initLineSearch = InitialQuadratic(αmax=100., snap2one=(.75, 1.5,), αmin=exp(-5))
algo = BFGS
singleRunTime = 60; singleRunIters = 2000
singleRunTime = 3; singleRunIters = 200
function adjustY(initX, target; pathOfYs=nothing, fitMethod=ScatteringTransform.defaultOptim)

end
"""
    curY = InterpolateInOutputDomain(curX, curY, target, St)
set the current target equidistant between the mean trained, the target, and the
current value, adjusted to have the same norm.
"""
function InterpolateInOutputDomain(curX, curY, target, St)
    meanY = mean(ScatteringTransform.flatten(St(curX)), dims=2)
    renormedTarget = target .* norm(meanY) / norm(target)
    curRenormed = curY .* norm(meanY) / norm(curY)
    return 1 / 3 * (meanY + curRenormed + renormedTarget)
end

if pathOfYs !== nothing
    curY = pathOfYs[:,end]
    pathOfYs = cat(pathOfYs, zeros(size(curY, 1), nAltern + 1), dims=2)
    startAt = size(pathOfYs, 2)
else
    pathOfYs = zeros(size(curY, 1), nAltern + 1)
    pathOfYs[:, 1] = target
    curY = target
    startAt = 0
end

if errPathX !== nothing
    append!(errPathX, Array{Array{Float64,1}}(undef, nAltern + 1))
else
    errPathX = Array{Array{Float64,1}}(undef, nAltern + 1)
end

if optsX !== nothing
    append!(optsX, Array{typeof(optX)}(undef, nAltern + 1))
    curX = optsX[startAt]
else
    optsX = Array{typeof(optX)}(undef, length(errPathX))
end

# fit with the initial target
obj∇x! = ScatteringTransform.makeOptimGradient(x -> obj(x, curY))
optX = ScatteringTransform.defaultOptim(obj∇x!, curX,
                                        singleRunIters, 60 * singleRunTime,
                                        true, algo, lineSearch; show_every=1,
                                        alphaguess=initLineSearch)
pathOfDescent, err, opt = fitReverseSt(singleRunIters, curX, obj=x -> obj(x, curY);
                                       errTol=1e10 * obj(curX, curY),
                                       timeLimit=Second(60 * singleRunTime));
plot(err[err .> 0],yscale=:log10)
curY = InterpolateInOutputDomain(curX, curY, target, St)
plot(curY)
size(pathOfDescent)
pathOfDescent = pathOfDescent[:,:,:,err .> 0]
err = err[err .> 0]
singleRunTime = 8 * 60; singleRunIters = 100000
pathOfDescent, err, opt = continueTrain(singleRunIters, pathOfDescent, err,
                                        opt=opt, obj=x -> obj(x, curY),
                                        errTol=1e10 * obj(curX, curY),
                                        timeLimit=Second(60 * singleRunTime),
                                        adjust=true);
pathOfDescent, err, opt = continueTrain(3, pathOfDescent, err,
                                        opt=opt, obj=x -> obj(x, curY),
                                        errTol=1e10 * obj(curX, curY),
                                        timeLimit=Second(60 * singleRunTime),
                                        adjust=true);
plot(err)
tup isa Tuple
typeof(optX)
optX isa Optim.MultivariateOptimizationResults
function extractThings(optX::O) where {O <: Optim.MultivariateOptimizationResults}
    err = [x.value for x in optX.trace]
    curX = optX.minimizer
    return optX, err, curX
end
function extractThings(listOfThings::T) where {T <: Tuple}
    return listOfThings
end

println(optX.stopped_by)
startAt + i + 1
optsX[startAt + i + 1] = optX
errPathX[startAt + i + 1] =
curX = optX.minimizer
plot(curX[:,1,4])

curY = InterpolateInOutputDomain(curX, curY, target, St)
plot(curY)
if size(pathOfYs, 2) == nAltern + 1 # second
    pathOfYs[:,2] = curY
end

for i = 1:nAltern
    println("-------------------------------------------------------------------------")
    println("Now starting i=$i")
    println("off the target by $(norm(curY - target .* norm(curY) / norm(target)))")
    println("-------------------------------------------------------------------------")
    obj∇x! = ScatteringTransform.makeOptimGradient(x -> obj(x, curY))
    # fit with the current target
    optX = ScatteringTransform.defaultOptim(obj∇x!, curX,
                                            singleRunIters, 60 * singleRunTime,
                                            true, algo, lineSearch,
                                            show_every=1, alphaguess=initLineSearch)
    optsX[startAt + i + 1] = optX
    errPathX[startAt + i + 1] = [x.value for x in optX.trace]
    println(optX.stopped_by)
    curX = optX.minimizer
    # adjust the target towards the median of the
    curY = InterpolateInOutputDomain(curX, curY, target, St)
    pathOfYs[:, startAt + i + 1] = curY
end

plot(curX[:,1,7:10])
# adjust the target a bit
obj∇y! = ScatteringTransform.makeOptimGradient(y -> obj(curX, y))
optY = ScatteringTransform.defaultOptim(obj∇y!, curY, 500, 60 * 2)
errPathCurY = [x.value for x in optY.trace]
optsY = Array{typeof(optY)}(undef, nAltern)
optsY[i + 1] = optY
curY = optY.minimizer
# set curY to be the average of the learned objective and the real target
curY = mean.(curY, target)
plot([curY,target], labels=["moved to" "original"])
plot(target)
St(curX)
plotSecondLayer(roll(curY))
updateTarget =
using Flux:mse
x = initPlus[:,:,:,1]
St.mainChain[1](x)[1]
w = St.mainChain[1]
size(w.weight)
sum(mse(a[1], a[2]) for a in zip(St(initPlus[:,:,:,1]), tmpRolled))
f(x) = hand(x, target)
roll(ScatteringTransform.flatten(St(initPlus[:,:,:,1])), St)
map(size, St(initPlus[:,:,:,1])[:,1].output)
roll(target, St)
(f(thi.minimizer))
f(initPlus[:,:,:,1])
remakeObj
thi = ScatteringTransform.defaultOptim(obj∇!, initPlus[:,:,:,1], 500, 60 * 4)
fitUsingOptim
adjustTarget
macro makeOptimFlux(f)
    function obj∇!(F, ∇, x)
        y, back = pullback(f, x)
        if ∇ != nothing
            ∇[:] = back(y)[1]
        end
        if F != nothing
            return y
        end
    end
end

macro makeJointObj(target::AbstractArray, st, normalize=st.normalize,
              λ=1e-10)
    obj(x) = sum(mse(a[1], a[2]) for a in zip(st(x), target)) + λ *
                                                          norm(x)
end
