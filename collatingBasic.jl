using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Plots

# make a small collection of squares
x = gpu(zeros(Float32, 32, 32, 3,1)); x[19:23, 16:20, 1,:] .= 1; x[19:20, 16:20,
                                                              2,:] .= 1;
x[19:23, 20:25, 3,:] .= 1;
x = CuArray(x);
# correct parameters, recursive interals, conv const tests
@time cL = collatingLayer(size(x), 3=>16)
cL.layers[3];
@time cL(x);
p = params(cL)
length(p.order)
@time ∇c = gradient(()->sum(abs(cL(x))), p)
fieldnames(typeof(p))
res = cL(x);
heatmap(x[:,:,3,1])
heatmap(res[:,:,6,1])
# debugging gradients
size(params(cL).order[1])
y, back = Zygote.forward(()->cL(x), params(cL));
size(y)
back(y)
y, back = Zygote.forward(()->cL.layers[1](x), params(cL));
size(y)
back(y)

yy, bacck = Zygote.forward(x->fftshift(x), randn(10,10))
bacck(yy)
gradient(x->fftshift(x)[1,1], randn(10,10))[1]

params(cL).params
fieldnames(typeof(params(cL)))
tmpPlan = plan_fft(cu(zeros(ComplexF32, 10,10)))
gradient(x->tmpPlan * x, cu(randn(10,10)))
y, back = Zygote._forward(x->tmpPlan * x, cu(randn(Float32, 10,10)))
tmpPlan * randn(10,10)

tmpPlan = plan_fft(zeros(Float32, 10,10,1,3), (1,2))
gradient(x->tmpPlan * x, randn(10,10))
y, back = Zygote._forward(x -> tmpPlan * x, randn(Float32, 10, 10))
tmpPlan * randn(10,10)




# fitting a specific example
input = gpu(randn(Float32, 32,32,1,30));
poolSize((5//4, 5//4), (32, 32, 1, 30))
# source = Chain(collatingLayer((32, 32, 1, 30), 1=>20),
#                (x)->maxpool(x, (5//4,5//4)),
#                collatingLayer((25, 25, 20, 30), 20=>15))
dest = Chain(collatingLayer((32, 32, 1, 30), 1=>5, useGpu=false),
             collatingLayer((32, 32, 5, 30), 5=>6, useGpu=false))
source = Chain(collatingLayer((32, 32, 1, 30), 1=>5, useGpu=false),
               collatingLayer((32, 32, 5, 30), 5=>6, useGpu=false))
size(params(dest).order[1])
p1 = params(source);
p2 = params(dest);

using LinearAlgebra
using Flux:mse
evalcb() = @show(sum([norm(params(dest).order[i] - params(source).order[i]) for
                      i=1:length(params(source).order)]))
evalcb() #if they're the same, this is zero
datt = [((randn(Float32, 32, 32, 1, 30)), gpu(randn(Float32,30))) for i=1:10]; size(datt)
loss(x,y) = norm(source(x) - dest(x))
@time loss(datt[1][1], datt[1][2])
@time ∇ = gradient(()->loss(datt[1][1], datt[1][2]), params(source))
@time ∇ = gradient(()->norm(source(datt[1][1]) - dest(datt[1][1])), params(dest))
norm(source(x) - dest(x))
heatmap(∇[p2.order[3]][1,1,:,:])
heatmap(∇[p2.order[1]][1,1,:,:])
heatmap(p2.order[3][1,1,:,:])
plot(heatmap(p1.order[1][1,1,:,:]), heatmap(p2.order[1][1,1,:,:]))
plot(∇[p2.order[2]])
size(∇[p2.order[3]])
size(p2.order[1])
heatmap(p2.order[1][1,1,:,:]-p1.order[1][1,1,:,:])
initialStateDest = p2.order[1][1,1,:,:];
heatmap(p1.order[1][1,1,:,:])
fieldnames(typeof(∇.grads))
evalcb() = @show(loss(datt[1][1], datt[1][2]))

examples = [(randn(Float32, 32, 32, 1, 30), randn(Float32,50)) for i=1:1000];
@time Flux.train!(loss, params(dest), examples, ADAM(), cb=evalcb)
# the loss
loss(examples[1][1], examples[1][2])
loss(datt[1][1], datt[1][2])

plot(heatmap(p1.order[1][1,1,:,:], title="match this"),
     heatmap(p2.order[1][1,1,:,:], title = "fitting"),
     heatmap(∇[p2.order[1]][1,1,:,:], title="gradient"))
plot(heatmap(p1.order[3][1,1,:,:], title="match this"),
     heatmap(p2.order[3][1,1,:,:], title = "fitting"),
     heatmap(∇[p2.order[3]][1,1,:,:], title="gradient"))

tmp = Flux.glorot_uniform(1,1,980,15);
heatmap(tmp[1,1,:,:])

using LinearAlgebra
destSingle = collatingLayer((32, 32, 3, 30), 3=>5, useGpu=false)
sourceSingle = collatingLayer((32, 32, 3, 30), 3=>5, useGpu=false)
p1 = params(sourceSingle);
p2 = params(destSingle);
loes(x,y) = norm(sourceSingle(x) .- destSingle(x))
datt = [((randn(Float32, 32, 32, 3, 30)), randn(Float32,30)) for i=1:10]; size(datt)
@time loes(datt[1][1], datt[1][2])
@time ∇ = gradient(()->loes(datt[1][1], datt[1][2]), p2)
@time ∇ = gradient(()->norm(destSingle(datt[1][1]) - sourceSingle(datt[1][1])), p2);
examples = [(randn(Float32, 32, 32, 3, 30), randn(Float32,50)) for i=1:100];
evalcb() = @show(sum([norm(params(destSingle).order[i] - params(sourceSingle).order[i]) for
                      i=1:length(params(sourceSingle).order)]))
evalcb()
Flux.train!((x,y)->norm(destSingle(x) - sourceSingle(x)), params(destSingle), examples, ADAM(), cb=evalcb)



X = randn(10,10)
pfX = plan_fft(X)
gradient((X)->pfX \ (pfX * X), X)




# check conv to see whether it is going both cross channel or not
conv
