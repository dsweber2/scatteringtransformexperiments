using Revise
using Plots
using ScatteringTransform, Flux, FourierFilterFlux
using Wavelets, FFTW
using ContinuousWavelets
t = range(-π,stop=π,length=2048)

ϡ = stFlux((length(t),1,1),2,σ=abs, dType=Float64,
           normalize=true, decreasing=[2,3,2], 
           averagingLength=[3,2,4], cw=Morlet(3π)) #\sampi
ϡ = stFlux((length(t),1,1),2,σ=abs, dType=Float64,
           normalize=true,decreasing=[2,3,2],
           averagingLength=[4,2,4]) #\sampi
a(t) = 1 .+ t +t.^2 - 1/3 * t.^3
plot(t,a(t))
gauss(t,μ,σ) = exp.(-(t.-μ).^2 ./ σ^2)
b(t) = gauss(t,-.5,.5) + .5gauss(t,2.5,1)
a(t) = b(t)
plot(t,b(t))
ϕ(t) = π/2 .+ 5 .*t.^2
φ(t) = 20*t .+ 20 ./ t.^2#\varphi
plot(t, φ(t))
plot(t,cos.(φ(t)))
x = reshape(a(t) .* cos.(ϕ(t)), (length(t),1,1))
y = reshape(a(t) .* cos.(φ(t)), (length(t),1,1))
p1 = plot(t, [a(t) -a(t) a(t) .* cos.(ϕ(t))],legend=false,title="quadratic")
p2 = plot(t, [a(t) -a(t) a(t) .* cos.(φ(t))],legend=false, title="chirp")
plot(p1,p2)
savefig("CompareOrigQuadraticChirpPhasesGaussians.png")
X = ϡ(x)
Y = ϡ(y)
plot(heatmap(X[1][:,:,1]', title="quadratic phase",c=:viridis), heatmap(Y[1][:,:,1]', c=:viridis, title="chirp phase"))
savefig("CompareFirstLayerQuadraticChirpPhasesGaussians.png")
gr()
p1 = plotSecondLayer(X, title="quadratic chirp",xVals=(.0015, .997), colorbar=false, logPower=false)
savefig("SecondLayerQuadraticChirpGaussian.png")
p2 = plotSecondLayer(Y, title="Hyperbolic chirp", xVals=(.0015, .997), colorbar=false, logPower=false)
savefig("SecondLayerHyperbolicChirpGaussian.png")
plot(p1, p2)
savefig("CompareSecondLayerQuadraticChirpPhasesGaussians.png")
p2
envelope = ϡ(reshape(a(t), (length(t),1,1)))
pyplot(); using LaTeXStrings
heatmap(envelope[1][:, :, 1]',title="scalogram of the shared envelope (two gaussians)")
savefig("envelopeTwoGaussians.png")
heatmap(envelope[1][:,:,1]',title="shared envelope "* L"a=1 + t +t^2 - \frac{1}{3}t^3")
savefig("envelopeA.png")
heatmap(Y[2][:,:,5,1]')

resDog = st(x)
plotSecondLayer(resDog)
plotSecondLayer(broadcast(x->abs.(x),resDog))

# what does it do on a delta spike?
δ = zeros(1024,1,1); δ[512] = 1.0
resδ = st(δ)
plotSecondLayer(resδ)

ff(;args...) = args
ab = ff(σ=3)
:σ in keys(ab)
