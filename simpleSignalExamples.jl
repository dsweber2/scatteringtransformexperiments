# Setting up the random shift distributions
using Random, Distributions
function generateData(N)
    d1=DiscreteUniform(16,32); d2=DiscreteUniform(32,96);

    # Making cylinder signals
    cylinderNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(N);
    for k=1:N
        cylinderNew[a[k]:b[k],k]=(6+η[k])*ones(b[k]-a[k]+1);
    end
    cylinderNew += randn(128,N);     # adding noise

    # Making bell signals
    bellNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(N);
    for k=1:N
        bellNew[a[k]:b[k],k]=(6+η[k])*collect(0:(b[k]-a[k]))/(b[k]-a[k]);
    end
    bellNew += randn(128,N);         # adding noise

    # Making funnel signals
    funnelNew=zeros(128,N);
    a=rand(d1,N); b=a+rand(d2,N);
    η=randn(N);
    for k=1:N
        funnelNew[a[k]:b[k],k]=(6+η[k])*collect((b[k]-a[k]):-1:0)/(b[k]-a[k]);
    end
    funnelNew += randn(128,N);       # adding noise


    test = cat(cylinderNew, bellNew, funnelNew, dims=2);
end

"""
    roundAssign!(to,v, start, i)
given an array `to` and a vector `v` shorter in one dimension, in the `i`th
column, add `v` starting at location `start`.
"""
function roundAssign!(to,v, start, i)
    to[start:min(start+length(v)-1, 128), i] +=
        v[1:(min(length(v), 1 + 128-start))]
    if start+ length(v) > 128
        to[1:(start+length(v) -129), i] += v[(128+2-start):end]
    end
end

function generatePeriodicData(N;rng=MersenneTwister(2))
    ℓ =DiscreteUniform(16,80); #\ell length
    s = DiscreteUniform(1,128) # starting location
    fullCollect = randn(rng, 128, 3*N)
    genCyl(len, k=6.0, M=128) = fill(k, len)
    # cylinder examples first
    for i = 1:N
        start = rand(rng, s)
        cyl = genCyl(rand(rng,ℓ))
        roundAssign!(fullCollect, cyl, start, i)
    end
    plot(mean(fullCollect[:,1:N],dims=2))
    plot!(fullCollect[:,1:5:N])
    genFun(len, k=6.0, M=128) = k * collect(len-1:-1:0)/(len-1)
    # funnel examples next
    for i = 1+N:2N
        start = rand(rng, s)
        fun = genFun(rand(rng,ℓ))
        roundAssign!(fullCollect, fun, start, i)
    end
    plot(mean(fullCollect[:, 1+N:2N], dims=2))
    plot!(fullCollect[:,(N+5):5:2N])
    genBell(len, k=6.0, M=128) = k * collect(0:len-1)/(len-1)
    # Bell examples next
    for i = 1+2N:3N
        start = rand(rng, s)
        bell = genBell(rand(rng,ℓ))
        roundAssign!(fullCollect, bell, start, i)
    end
    return fullCollect
end
