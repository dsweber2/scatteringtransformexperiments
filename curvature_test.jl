using Statistics
using Logging; global_logger(ConsoleLogger(stderr, Logging.Debug))
# * Testing curvature
# first, we load the cartoonlike package, which is a simple way of
# creating curves. Here we create a family of curves with curvature
# varying from a positive value to a negative value

using Revise
using CUDA, Zygote, Plots, cartoonLike, FFTW, LinearAlgebra, collatingTransform, Flux, LinearAlgebra
nPoints = 5
curveExamples = distinguishing_curvature(nPoints,cart = cartoonLikeImage{Float32}(xSize=50,ySize=50))
plot(heatmap(curveExamples[:,:,3]), heatmap(curveExamples[:,:,11]), layout=(1,2))
curveExamples = Float32.(reshape(curveExamples, (size(curveExamples)[1:2]..., 1,
                                                 size(curveExamples, 3)))) |> gpu

# Now that we have a collection of test images, lets see what a collating layers
# does. We'll consider just the first channel, since initially every channel is just
# random sums of each frame element
stack = collatingLayer(size(curveExamples), 1=>1, scale=4)
@time oneLayered = stack(curveExamples); size(oneLayered)
plot(heatmap(oneLayered[:,:,1,3]), heatmap(oneLayered[:,:,1,6]), heatmap(oneLayered[:,:,1,9]), heatmap(oneLayered[:,:,1,11]), layout=(2,2))
# You can more or less make out the original curves. But this is no different
# than doing one dense layer on the output with random weights. Here's an example
# after a couple of untrained layers, which is distinctly a collating transform.
stack = Chain(collatingLayer(size(curveExamples), 1=>1),
              MaxPool((2,2)),
              collatingLayer((50,50,49,11), 49=>1),
              MaxPool((2,2)),
              collatingLayer((25,25,49,11), 49=>1))
@time transformed = stack(curveExamples);
plot(heatmap(transformed[:,:,6,7], title = "3rd layer scattering"), heatmap(curveExamples[:,:,1,7]))
# Without any training, we're just getting a random blurring of the
# coefficients. We don't have classes really in this example, but we do have a
# regression problem of sorts. What we want to do is maximize the sparsity while
# still being able to distinguish the curvature. If we denote the last layer
# collating layer by c, and the curvature \kappa, one way of doing this is
# \begin{center}
# \min \|c\|_1 + \sum_{i=1}
# \end{center}
using Flux: mse, crossentropy, @epochs
κ = cu(Float32.(I(11)).* ones(Float32, 11, 11))
size(curveExamples)
ch = [11,11,11]
n = 1
firstPool = RationPool((3//2,3//2))
secondPool = RationPool((5//4,5//4))

startSize = size(curveExamples)[1:2]
firstSize = poolSize(firstPool,startSize)
secondSize = poolSize(secondPool,firstSize)

nShears = 17
nScales = 2

nonlin = abs

# only selecting the coefficients in the neighborhood of the curve
center =floor.(Int,secondSize ./ 2)
offset = ceil.(Int, 5*secondSize[1]./startSize[1])
centerSize = [(x.-offset):x+offset for x in center]

stack = Chain(collatingLayer((startSize..., 1, n), 1=>ch[1], σ=nonlin,
                             init=iden_perturbed_gaussian, scale=nScales),
              firstPool,
              collatingLayer((firstSize..., ch[1]*nShears, n), ch[1]*nShears=>ch[2],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              secondPool,
              collatingLayer((secondSize..., ch[2]*nShears, n), ch[2]*nShears=>ch[3],
                             σ=nonlin, init=uniform_perturbed_gaussian,
                             scale=nScales),
              (x) -> x[centerSize..., :, :],
              (x) -> reshape(x, (prod(length.(centerSize))*nShears*ch[3], :)),
              Dense(prod(length.(centerSize))*nShears*ch[3], 11, nonlin), 
              softmax) |> gpu
@time ex = stack(curveExamples[:,:,:,1:1]);

gradient(()->sum(stack(curveExamples[:,:,:,1:1])), params(stack))
x = curveExamples[:,:,:,1:1]
stack.layers[1](x)[1]
pL(ii) = params(stack.layers[1:ii])
tran(x,ii) = reduce((x,st) ->st(x),stack.layers[1:ii],init=x)
tran(x,ii) = Chain(stack.layers[1:ii]...)(x)
length(stack.layers)
ii=3; gradient(() -> tran(x,ii)[1], pL(ii))

# to get a sense of how focused the area we're looking at is (roughly)
centerFull =floor.(Int, startSize ./ 2)
offsetFull = 5
centerSizeFull = [(x.-offset):x+offset for x in centerFull]
plot(heatmap(cpu(curveExamples[centerSizeFull...,1,3])), 
     heatmap(cpu(curveExamples[centerSizeFull...,1,11])), layout=(1,2))

# a loss function which encourages both fitting the curve (crossentropy) and
# having a small 1 norm in the dense layer 
function loss(x,y)
    activ = Flux.activations(stack, x)
    return crossentropy(activ[end], y) + .005 .* norm(activ[end-3], 1)/norm(x,2)
end
function simpleLoss(x,y)
    return crossentropy(stack(x), y)
end
function buildRecord(stack, nEpochs)
    p = params(stack).order
    return [zeros(Float32, size(x)..., nEpochs+1) for x in p]
end
function expandRecord!(record, nEpochs)
    for ii in 1:length(record)
        adding = zeros(Float32, size(record[ii])[1:end-1]..., nEpochs)
        record[ii] = cat(record[ii], adding, dims=ndims(record[ii]))
        size(record[ii])
    end
end
function addCurrent!(record, stack,ii)
    p = params(stack).order
    for (kk,x) in enumerate(p)
        ax = axes(x)
        record[kk][ax..., ii+1] = x
    end
end
@time loss(curveExamples[:, :, :, 1:1], κ[:, 1])
@time ∇ = gradient(()->loss(curveExamples[:, :, :, 1:1], κ[:, 1]),
                   params(stack))

@time ∇ = gradient(()->simpleLoss(curveExamples[:, :, :, 1:1], κ[:, 1]),
                   params(stack)) 

randExamples(k) = [(curveExamples[:,:,:, i:i], κ[:,i]) for i in rand(1:(nPoints*2+1))]
nEpochs = 120; nSamples = 30
fittingCurve = zeros(nEpochs);
crossEnt = zeros(nEpochs);
record = buildRecord(stack, nEpochs);
addCurrent!(record,stack,0)
evalcb() = @show(sum([loss(curveExamples[:,:,:,i:i], κ[:,i]) for i=1:11]))

for epoch=1:nEpochs
    if crossEnt[end]>0
        append!(crossEnt, zeros(nEpochs))
        append!(fittingCurve, zeros(nEpochs))
        expandRecord!(record, nEpochs)
    end
    Flux.train!((x,y)->simpleLoss(x,y), 
                params(stack), 
                randExamples(nSamples), ADAM())
    putIn = length(crossEnt)-nEpochs
    fittingCurve[putIn + epoch] = sum([loss(curveExamples[:,:,:,i:i], κ[:,i]) 
                                       for i=1:11])
    crossEnt[putIn + epoch] = sum([crossentropy(stack(curveExamples[:,:,:,i:i]),
                                                κ[:,i]) for i=1:11])
    println("$(putIn+epoch), $(fittingCurve[putIn + epoch]), $(crossEnt[putIn + epoch])")
    addCurrent!(record,stack,putIn+epoch)
end

maximum(record[1])
plot([crossEnt, fittingCurve], labels=["cross entropy" "net score"])
plot(crossEnt)
plot([plotCollatingLayer(record, ii, 80) for ii=1:3]...,layout=(1,3))
heatmap(record[end])
record[end-3])
i=3; activ = Flux.activations(stack, curveExamples[:,:,:,i:i]);
acit
size(activ[end])
activ[end]
size(activ[1])
size(activ[end-4])
heatmap(cpu(activ[1][:,:,6,1]))
heatmap(cpu(activ[end-4])[:,:,4,1])

p = params(stack);
length(p.order)
size(p.order[2])
p.order[1]
p.order[2]
k=8; (maximum(p.order[k]),size(p.order[k]))

size(record[3][1,1,1,:,:,1])
size(record[2])
function plotCollatingLayer(record,ii, epoch)
    plot(heatmap(record[2*ii-1][1,1,1,:,:,epoch+1], title="layer $(ii)"),
         plot(record[2*ii][:,epoch+1],legend=false,tickfontsize=2, colorbar=true),layout=grid(2,1,heights=[.7,.3]))
end

size(record[1])
length(record)
function plotCollatingLayer(record,ii, epoch)
    plot(heatmap(record[2*ii-1][1,1,1,:,:,epoch+1], title="layer $(ii)"),
         plot(record[2*ii][:,epoch+1],legend=false,tickfontsize=2, colorbar=true),layout=grid(2,1,heights=[.7,.3]))
end

anim = Animation()
for ep = 0:nEpochs
    plot([plotCollatingLayer(record, ii,ep) for ii=1:3]...,layout=(1,3))
    frame(anim)
end
maximum(diff(record[5][1,1,1,:,:,:],dims=3))./mean(record[5][1,1,1,:,:,:])
maximum(diff(record[3][1,1,1,:,:,:],dims=3))./mean(record[3][1,1,1,:,:,:])
maximum(diff(record[1][1,1,1,1,:,:],dims=2))./mean(record[1][1,1,1,1,:,:])
heatmap(diff(record[1][1,1,1,1,:,:],dims=2))
gif(anim, "tmp.gif", fps=2)
