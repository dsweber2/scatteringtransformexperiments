import numpy as np
import h5py
import pickle
from sklearn import svm


from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.pipeline        import Pipeline
from sklearn.decomposition   import PCA
from sklearn                 import linear_model as lm

# import the data
startPoint = 0
endPoint = 10
dataSet = "FashionMNIST"
classMethod = "pca_l1regularized"
baseDir = "/fasterHome/workingDataDir/shattering/"
labelFile = baseDir + dataSet +"/" + "Labels.h5"
names = ["shattered"] # "abs", "relu", "tanh", "altSlopes", "noTransform", 
transformNames = (len(names)-1)*["poolBy65"] + ["data"]
res_files = [baseDir + dataSet + "/" + x + "Results.h5" for x in names]
source_files = (len(names)-1)*[baseDir + dataSet + "/" +
                               "collatingVersion.h5"] + [baseDir + dataSet +
                                                         "/" +
                                                         "kymatio2"+dataSet+".h5"]
final = False

if classMethod == "pcaFull_rbf_svm":
    def pipeSteps(nExamples, dataSize):
        return [('pca', PCA(whiten=True, copy=True,
                     n_components=min(300,
                                      nExamples-10, dataSize))),
         ('svm', svm.SVC(kernel = 'rbf', gamma = 'auto',
                         verbose=False))]
elif classMethod == "pca300_rbf_svm":
    def pipeSteps(nExamples, dataSize):
        return [('pca', PCA(whiten=True, copy=True,
                            n_components=min(300,
                                             nExamples-10, dataSize))),
                ('svm', svm.SVC(kernel = 'rbf', gamma = 'auto',
                                verbose=False))]
elif classMethod == "linearSVM":
    def pipeSteps(nExamples, dataSize):
        return [('svm', svm.SVC(kernel = 'linear'))]
elif classMethod == "pca_l1regularized":
    def pipeSteps(nExamples, dataSize):
        print(f"{min(300, nExamples-10, dataSize)} nExamples = {nExamples-10}, dataSize ={dataSize}")
        return [('pca', PCA(whiten=True, copy=True,
                            n_components=min(300,
                                             nExamples-10, dataSize))),
                ('logit', lm.LogisticRegressionCV(penalty = 'l1', solver = 
                                                  'saga', max_iter = 10000,
                                                  multi_class = 'multinomial',
                                                  refit = False, cv = 3,
                                                  n_jobs = 10))]
$(logit)=Pipeline(steps = [($(pcaN), PCA(whiten=True, copy=False,
                                         n_components=$(nKeep))),
                        ($(logitN), 
                         lm.LogisticRegressionCV(penalty = $(penaltyType), 
                                                 solver = $(solver), 
                                                 Cs = $(Cs), 
                                                 max_iter=$(max_iter),
                                                 cv=$(cv), n_jobs=$(n_jobs),
                                                 refit=$refit))])
elif classMethod == "l1regularized":
    def pipeSteps(SizeCV, dataSize):
        return [('logit', lm.LogisticRegressionCV(penalty = 'l1', solver = 
                                                  'saga', max_iter = 10000,
                                                  multi_class = 'multinomial',
                                                  refit = False, cv = 3,
                                                  n_jobs = 10))]
# load the data
for (iFile, typeName) in enumerate(names):
    f = h5py.File(source_files[iFile], 'r')
    db = f[transformNames[iFile] + "/" + typeName]
    if typeName =="shattered":
        labels = f["data/labels"][:]
    else:
        f2 = h5py.File(labelFile)
        labels = f2["labels"][:]
        f2.close()

    stCoeff = db[:,:]
    f.close()
    dTrain, dTest, lTrain, lTest = train_test_split(stCoeff, labels,
                                                    random_state = 4025)
    resultFile = h5py.File(res_files[iFile], "a")

    # set the size of the training set via setting the cv number of folds
    N = lTrain.shape[0]
    SizeCV = [int(np.floor(50*5**x)) for x in
              np.linspace(0,np.log(dTrain.shape[0]/50)/np.log(5),num=10)]
    nRepeats = [int(np.ceil(N/x)) for x in SizeCV[0:-1]]
    nRepeats.reverse()
    SizeCV.reverse()
    nRepeats = nRepeats[startPoint:endPoint]
    SizeCV = SizeCV[1+startPoint:(1+endPoint)]
    print(nRepeats)
    print(SizeCV)

    if final:
        print("Running Final, On " + names[iFile] + " i.e. " + str(iFile))
        classifier = Pipeline(steps = pipeSteps(SizeCV[0], stCoeff.shape[1]))
        classifier.fit(dTrain, lTrain)
        dset = resultFile.require_dataset(classMethod + "/SampleRate52500",
                                          (1,),
                                          dtype = np.dtype('float32'))
        dset[:] = classifier.score(dTest, lTest)
        fileObject = open(baseDir + dataSet + "/" + names[iFile] + classMethod + 
                          "finalClassifier.p", 'wb')
        pickle.dump(classifier, fileObject)
    else:
        for (i,x) in enumerate(nRepeats):
            print("On " + str(x) + " i.e. " + str(i) + " out of " +
                  str(len(nRepeats)))
            repeater = StratifiedKFold(x)
            reRepeater = repeater.split(dTrain, lTrain)
            flippedFolder = ((y[1], y[0]) for y in reRepeater) # a generator
            # with large test sets and small train sets
            classifier = Pipeline(steps = pipeSteps(SizeCV[i], stCoeff.shape[1]))
            scores = cross_val_score(classifier, dTrain, lTrain,
                                     cv=flippedFolder, n_jobs=2)
            dset = resultFile.require_dataset(classMethod + "/SampleRate" + str(x),
                                              scores.shape,
                                              dtype = scores.dtype)
            dset[:] = scores
            resultFile.flush()
        print("finished " + str(iFile))
        resultFile.close()
