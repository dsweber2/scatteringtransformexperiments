using Plots

using Revise
using CUDA, Zygote, Flux, FFTW
using collatingTransform
using Test
using LinearAlgebra
using ScikitLearn
using JLD2, Statistics
using Flux:Optimiser,mse
using FileIO, JLD2
@load "../shape_train.jld2"
size(cylinder)
@sk_import linear_model: LogisticRegressionCV
dat = reshape(cat(cylinder, bell, funnel, dims=2), (128,1,300));  #dat = dat .- mean(dat,dims=1);
include("../simpleSignalExamples.jl")
#dat = reshape(generateData(100), (128,1,300));
test = reshape(generateData(100), (128,1,300));
#test = test .- mean(test,dims=1);
y = [fill(0,100); fill(1,100); fill(2,100)];

# fit logistic regression on the raw dat
clf_raw = LogisticRegressionCV(penalty="l1",solver="saga",multi_class="multinomial", max_iter=10000)
fit!(clf_raw, dat[:,1,:]',y)
sum(predict(clf_raw,dat[:,1,:]') .==y)/length(y)
sum(predict(clf_raw,test') .==y)/length(y)
# with raw dat, its actually possible to predict w/87% accuracy
# this goes to 91-93% if you subtract the mean from each example
# this problem may be easier to predict on the raw dat than would be good for comparison
plot(dat[:,1,120])
k = 1:3; plot((clf_raw.coef_[k,:] .- clf_raw.intercept_[k])')
clf_raw.intercept_
mean(dat[:,1,1:100])
mean(dat[:,1,101:200])
mean(dat[:,1,201:300])

# fitting the absolute value of the fourier transform
clf_avft = LogisticRegressionCV(penalty="l1",solver="saga",multi_class="multinomial", max_iter=10000)
fit!(clf_avft, abs.(rfft(dat[:,1,:],1)'),y)
sum(predict(clf_avft,abs.(rfft(dat[:,1,:],1)')) .==y)/length(y)
sum(predict(clf_avft,abs.(rfft(test,1)')) .==y)/length(y)
# avft actually does abysmally bad?? 62& on the train set and 58% on the test set
# using real doesn't fix that
plot(clf_avft.coef_') # as expected, all of this is in the low frequency range


# lets get a notion of where we need Fourier domain coverage
plot(abs.(rfft(dat,1)[:,1,150]))
F = svd(rfft(dat,1)[:,1,:] .- mean(rfft(dat,1)[:,1,:],dims=2));
plot(abs.(F.S)) # so really the first 6 ish components are the most important
plot(abs.(F.U[:,1:5]), title="first 5 PCA components of the rfft of the dat") # even after subtracting the mean, very much the low frequency
savefig("gradientFigures/pcaComponents.png")
# start the scattering transform classification
clf = LogisticRegressionCV(penalty="l1",solver="saga",multi_class="multinomial", max_iter = 10000, cv=5,Cs = 10 .^(range(-1.5,stop=-1,length=4))) #choosing the range of Cs based on what actually fit well over the range -4 to 4
plot(dat[:,1,100])

st = scatteringTransform(size(dat), 2, useGpu=false, dType=Float64, normalize=true, decreasings=[2,3,2],averagingLengths=[1,2,4])
wave1, wave2, wave3 = getWavelets(st);
(size(wave1,2), size(wave2,2), size(wave3,2))
plot(plot(abs.(wave1), legend=false,title="layer 1 wavelets"),
     plot(abs.(wave2), title="layer 2 wavelets", legend=false),
     plot(abs.(wave3), title="layer 3 averaging", legend=false))
savefig("gradientFigures/usedWavelets.pdf")
 # rather more blocky than we may want, but sufficient representation

stOne = scatteringTransform((128,1,1), 2, useGpu=false, dType=Float64, normalize=true, decreasings=[2,3,2], averagingLengths=[1,2,4])

# plot the wavelets in the space domain
plot(abs.(wave1[:,1]))
k=15; wav = circshift(irfft(wave1,128*2,1)[:,k],(64,))[1:128]
plot(plot(wav,title="Wavelet $(k)",legend=false), plot(abs.(wave1[:,k]),legend=false),layout=(2,1))

for k = 1:size(wave1,2)
    wav = circshift(irfft(wave1,128*2,1)[:,k],(64,))[1:128]
    plot(plot(wav,title="Wavelet $(k)",legend=false), plot(abs.(wave1[:,k]),legend=false),layout=(2,1))
    savefig("fitAllPaths/waveletFirstLayer$(k).pdf")
    savefig("fitAllPaths/waveletFirstLayer$(k).png")
end

n=2048; histogram(irfft(exp.(im*rand(n+1)*2π),2*n))
plot(irfft(ones(129), 256))
# see what the st does to the wavelets themselves
k = 28; stWave = stOne(reshape(circshift(irfft(wave1,128*2,1)[:,k],(60,))[1:128], (128,1,1)))
heatmap(stWave[1][:,:,1]')
savefig("stOfWavelets/firstLayer$(k).pdf")
savefig("stOfWavelets/firstLayer$(k).png")
plotSecondLayer(stWave, title="second layer of first layer wavelet $(k)")
savefig("stOfWavelets/secondLayer$(k).pdf")
savefig("stOfWavelets/secondLayer$(k).png")


@time res = st(dat)
flt = collatingTransform.flatten(res);
size(flt)
fit!(clf, flt', y)
sum(predict(clf,flt') .==y)/length(y)
test = reshape(generateData(100), (128,1,300));
testSt = st(reshape(test, (128,1,300)))
sum(predict(clf,collatingTransform.flatten(testSt)') .==y)/length(y) # 92-94% accuracy (not run to convergence)
# looking at what didn't get fit
classWrong = predict(clf,collatingTransform.flatten(testSt)') .!=y
findall(classWrong)
[predict(clf,collatingTransform.flatten(testSt)')[classWrong] y[classWrong]]
plot(test[:,1,classWrong][:,3])
# which level of regularlization did we use?
clf.C_
clf.Cs_
clf.n_iter_
coefs = roll(clf.coef_',st)
C = clf.intercept_
# getting the coefficients relevant for a specific class
cylinderCoef = coefs[:,1:1]
cylPaths = nonZeroPaths(cylinderCoef,allTogetherInOne=false);
bellCoef = coefs[:,2:2]
bellPaths = nonZeroPaths(bellCoef,allTogetherInOne=false);
funnelCoef = coefs[:,3:3]
funPaths = nonZeroPaths(funnelCoef, allTogetherInOne=false);
sum(abs.(coefs[pathLocs(0,:)]).>0)
sum(abs.(coefs[pathLocs(1,:)]).>0)
sum(abs.(coefs[pathLocs(2,:)]).>0) # only coefficients used to classify in the last layer. There are only 271 coefficients used between all 3 classes
# plot a summary of which paths are important for which
sz = size(coefs[2])
nonZeroLocs = zeros(sz[3:-1:2]...)
for (pathCollect, val) in ((cylPaths, 1), (bellPaths,2), (funPaths, 4))
    for p in pathCollect
        nonZeroLocs[p.indices[3][3:-1:2]...] += val
    end
end
using ColorSchemes
cgrad(:viridis)[0.5]
plotSecondLayer(coefs, c=:lightrainbow,toHeat = nonZeroLocs', title="nonzero paths in the second layer\n (1,black)=cylinder (2,gray)=bell (4,white)=funnel") # it appears that the paths chosen are pretty stable. primarily high frequency in the first layer and low frequency in the second are the important paths
# fixing the extra bit on the edge got rid of

savefig("importantPathsLayer2.pdf")

# let's try fitting the cylinder classification coefficients
save("fitCoefficients.jld2", "coefs", coefs)
@load "fitCoefficients.jld2"
# fitting the locations chosen for a given (correctly classified) cylinder
# signal
cylLocs = nonZeroPaths(coefs[:,1:1],
                       allTogetherInOne=true,wholePath=false);
allLocs = nonZeroPaths(coefs[:,1:3],
                       allTogetherInOne=true,wholePath=false);
allLocs = pathLocs{3}((nothing, nothing, any(allLocs.indices[3],dims=4)))
predict(clf,collatingTransform.flatten(testSt)')[5] # 5 was correctly
# classified, so we can use it
obj = makeObjFun(res[:,35:35], allLocs, stOne, false)
obj(test[:,:,5:5])
initGuess = cat(dat[:,1,104], dat[:,1,10], ones(128,1,1), randn(128,1,30),dims=3) # randn was a better starting location
# using Optim tools instead
optimRes = Array{Optim.MultivariateOptimizationResults,1}(undef, size(initGuess,3))
for kk=1:size(initGuess,3)
    println(kk)
    optimRes[kk], λ = fitUsingOptim(10000000000000, initGuess[:,:,kk:kk], allLocs, res[:,35:35], stOne,time=120,tryCatch=false,allowIncrease=false)
end
optimRes
err = [[x.value for x in oneTarget.trace] for oneTarget in optimRes]
plot(err, yscale=:log10, xlabel="iteration", labels=["bell ex 104" "cyl ex 10" "ones" ["randn" for i = 1:30]...],
     title="Fitting cylinder 35 at all used examples\nerror for $(size(initGuess,3)) different runs", ylabel="error")
saveTo = "fitSolutions/allCyl"
savefig(joinpath(saveTo,"error35.pdf"))
savefig(joinpath(saveTo,"error35.png"))
minimum(minimum.(err[3:end]))
μ = argmin(minimum.(err))
μ = argmin(minimum.(err[3:end])) # first set μ to something fair
optimRes[μ].minimum
tmp = randn(128,1,1); pert,newVal = perturb(optimRes[μ].minimizer,obj,.1,500)
perturbedRes, λ = fitUsingOptim(10000000000000, pert, allLocs, res[:,35:35], stOne, timeAl=120)
tooMany = 0
rate = .01
for j=1:1000
    println(j)
    global perturbedRes, λ, pert, tooMany, rate
    pert, newVal = perturb(perturbedRes.minimizer, obj, rate, 1000)
    if pert!=nothing
        perturbedRes, λ = fitUsingOptim(10000000000000, pert, allLocs, res[:,35:35], stOne,timeAl=120,tryCatch=false,allowIncrease=false)
        println(perturbedRes.minimum)
        tooMany=0
    else
        tooMany+=1
        if tooMany > 5
            rate *=.9
            tooMany= 0
        end
    end
end
run(`notify-send "Ding, fries are done"`)
save("/home/dsweber/PERTURBED.jld2", "perturbedRes", perturbedRes,)
randn(128,1,10) ./reshape([norm(w) for w in eachslice(randn(128,1,10),dims=3)], (1,1,10))
perturbedRes
pert, newVal = perturb(perturbedRes.minimizer, obj,.001)
function perturb(x, obj, stepSize = .01, K=100)
    tmp = randn(size(x)[1:end-1]...,K)
    tmp = stepSize*norm(x) * tmp ./ 
        reshape([norm(w) for w in eachslice(tmp, dims=3)], (1,1,K))
    perturbed = x .+ tmp
    errs = [obj(reshape(w,(size(w)...,1))) for w in eachslice(perturbed,dims=3)]
    μ = argmin(errs)
    if errs[μ] > obj(x)
        println("couldn't find a better location. Adjust stepSize or number of samples K")
        return nothing, nothing
    end
    return perturbed[:,:,μ:μ], errs[μ]
end
plot(plot(perturbedRes.minimizer[:,1,1], legend=false), plot(abs.(rfft(perturbedRes.minimizer[:,1,1])),legend=false))
k= 3; plot(plot(optimRes[k].minimizer[:,1,1], legend=false), plot(abs.(rfft(optimRes[k].minimizer[:,1,1])),legend=false))
comp = cat(res[:,35:35], stOne(optimRes[3].minimizer))
coefIndicator = scattered((zeros(size(res[:,35:35][0])), zeros(size(res[:,35:35][1])), allLocs.indices[3] .* .1*norm(res[:,35:35][2], Inf)))
scattered(coefs[:,1:1] .* (norm(res[:,35:35])))
comp = cat(res[:,35:35], stOne(perturbedRes.minimizer), coefIndicator)
plotSecondLayer(comp, title = "Second Layer All locations (non-pink) starting from a different cylinder,\n (white fit, black original, grey used locations)", toHeat = nonZeroLocs', c=:lightrainbow)
#saveTo = "fitSolutions/justCyl"
savefig(joinpath(saveTo, "SecondLayer35_err827.pdf"))
savefig(joinpath(saveTo, "SecondLayer35_err827.png"))
save(joinpath(saveTo, "actualExample35_start_fromEx10.jld2"), "res", perturbedRes)
heatmap(stOne(perturbedRes.minimizer)[1][:,:,1]',title="First Layer Cylinder locations only, err $(round(perturbedRes.minimum,digits=2))",
        xlabel="space",ylabel="frequency")
savefig(joinpath(saveTo, "FirstLayer35_err827.pdf"))
savefig(joinpath(saveTo, "FirstLayer35_err827.png"))
reNormed = norm(dat[:,1,35],Inf)/ norm(perturbedRes.minimizer[:,1,end],Inf) .* perturbedRes.minimizer[:,1,end]
using LaTeXStrings
plot(plot([dat[:,1,35] .- mean(dat[:,1,35])  reNormed],
          title="Fitting Cylinder example at all locations,\n renormalized and minus mean, err $(round(perturbedRes.minimum,digits=2))",legend=false),
     plot(abs.(rfft([dat[:,1,35] .- mean(dat[:,1,35]) reNormed], 1)), label=[L"original - mean" L"result\cdot\|original\|_{\infty}/\|result\|_{\infty}\cdot"],legend=:topright, title="Positive half of the Fourier coefficients"), 
     layout=(2,1))
savefig(joinpath(saveTo, "ActuallyFitPlotAdjusted35.pdf"))
savefig(joinpath(saveTo, "ActuallyFitPlotAdjusted35.png"))


initGuess = randn(128,1,1)
(1e-3-.06*1e-4)/(1000*.06*1e-4) # rough estimate of decay rate
opt = Optimiser(InvDecay(.01), ADAM(1e-3)); N=2000
opt = ADAM(.001); N=300
opt = Optimiser(InvDecay(1e-4),Momentum(100000)); N = 1000
opt = Optimiser(InvDecay(1e-2),Momentum(1e-7)); N=300
opt = Momentum(1e-7); N=300
opt = Momentum(1e-2); N = 1000
descent, err,opt = fitReverseSt(N, initGuess; opt=opt, obj=obj, keepWithin=1, stochastic=false); plot(err,yscale=:log10, legend=false)
plot(err,yscale=:log10, legend=false)
opt = Momentum(1e-8); N =300
opt = ADAM(1e-4); N =400; descent[:,:,end:end] += .0001*randn(128);
opt = Optimiser(InvDecay(.1),ADAM(.06*1e-4))
opt = ADAM(2e-4)
44/400
10000/112*60*60*4
N=floor(Int,10000/112*60*60*4); @time descentTmp, errTmp, opt = continueTrain(N, descent, err; opt=opt, obj=obj, stochastic=true, keepWithin=norm(dat[:,1,35]));
plot([err[1:7750+400] errTmp],yscale=:log10, legend=false)
plot([dat[:,1,35] circshift(descent[:,1,7750],0)*100])
argmin(err[k.+(1250:1700)])
k,kend = 2300,length(err); plot(plot(log.(err[k:kend]),legend=false, title="Error increasing then decreasing", xlims=(0,kend-k)), heatmap(diff(descent[:,1,k:kend],dims=2),colorbar=false,title="gradient"),layout=(2,1))
savefig("gradientFigures/cylinderExamples/peakingObjective.pdf")
heatmap(diff(descentTmp[:,1,450:kend],dims=2))
mkpath("gradientFigures/cylinderExamples")
transformPath(pod, p,st) = cat([st(reshape(x,(size(x)...,1)))[p] for x in
                                eachslice(pod,dims=3)]...,dims=2)
plot(plot(descentTmp[:,1,end],legend=false, title="fitting Cylinder coordinates from all ones"), plot(abs.(rfft(descentTmp[:,1,end:end], 1)),legend=false,title="positive half of the Fourier coefficients"),layout=(2,1))
savefig("gradientFigures/cylinderExamples/ActuallyFitPlot.pdf")

heatmap(abs.(rfft(descentTmp[:,1,2:end], 1)'), title="Descent Trajectory", ylabel="iteration", xlabel="space")
p1 = heatmap(stOne(descentTmp[:,1:1,end:end])[1][:,:,1]', title="First Layer Coordinates", xlabel="space",ylabel="frequency",c=cgrad([:blue, :orange], scale=:log))
savefig("gradientFigures/cylinderExamples/first_layer_coords.pdf")
plotSecondLayer(stOne(descent[:,:,end:end]), c=cgrad([:blue, :orange], scale=:log),
                title = "Second Layer Cylinder Fit")
savefig("gradientFigures/cylinderExamples/secondLayerPlot.pdf")
plotSecondLayer(stOne(initGuess),title="Second Layer Constant")

# second layer comparison between the result and the 
forComparison = scattered(map((x,y)->cat(x,y,dims=ndims(x)), stOne(descent[:,:,end:end]).result, res[:,35:35].result))
plotSecondLayer(forComparison,
                title = "Second Layer, fit (black) vs original (white), color giving fit size")



pathOfRes = transformPath(descent, cylLocs, stOne);
size(pathOfRes)
plot(pathOfRes[:,450:end], line_z=([450:size(pathOfRes,2)...])',
      color=:darkrainbow, legend=false, colorbar=true)
plot!(testSt[:,5:5][cylLocs],color=:black, legend=false,linewidth=3, title="In no particular order the values being fit and their path") #goes in
#the right direction for a while and then wildly overshoots
oneAta.indices[3]
oneAta = addNextPath(cylLocs)
initGuess = randn(128,1,1)
initGuess = ones(128,1,1)
obj = makeObjFun(res[:,8:8],stOne,false)
ran
opt = Momentum(10); N = 500
N = 28224;
@time descent, err = fitReverseSt(N, initGuess; opt=opt, obj=x->obj(x,cylLocs), keepWithin=10*norm(res[:,5]));plot(log.(err), legend=false)
@time descent, err = continueTrain(N, descent,err; opt=opt, obj=x->obj(x,cylLocs), keepWithin=10*norm(res[:,5]));plot(log.(err), legend=false)
plot(descent[:,1,end])
plot(descent3[:,1,end])
save("fit_startingAtOnes.jld2", "cylLocs", cylLocs,"err5",err3, "descent5", descent3, "err8",err, "descent8", descent)
descent2 = descent; err2=err; @load "fit.jld2"
err[end]/err[1] > 1e-4
maximum(descent[:,1,end:end])
totalPaths = pathsLeft(oneAta,cylLocs) # 36 is when things broke
opt = Momentum(10); N = 10000
pathsLeft(oneAta,cylLocs)
for ii=1:totalPaths
    global descent, oneAta, err
    println("$(pathsLeft(oneAta,cylLocs)), so far $(size(descent,3)) total iterations")
    oneAta = addNextPath(oneAta, cylLocs); pathsLeft(oneAta,cylLocs)
    descent, err = continueTrain(N, descent, err; opt=opt, obj=(x->obj(x,oneAta)), keepWithin=10*norm(res[:,5]));plot(log.(err), legend=false)
    if err[end]/err[1] > 1e-4
        println("not enough iterations early, this will go poorly")
        break
    end
end
plot(log.(err), legend=false)
size(descent)
save("fit.jld2","errCyl1",err,"cylLocs", cylLocs, "descent1", descent1, "errCyl2",err2, "descent2", descent2, "errCyl3",err3, "descent3", descent3)
x = round.(Int,range(1,stop=size(descent,3),length=10000))
obj(descent[:,1:1,end:end], cylLocs)
plot
p1 = heatmap(descent[:,1,:]',xlabel="space",ylabel="iteration",title="the descent path to the classification relevant Cylinder coordinates")
p2 = plot(descent[:,1,end],legend=false,title="final result")
p3 = plot(dat[:,1,5:5], title="Chosen Cylinder Example",legend=false)
p4 = plotSecondLayer(res[:,5])
plotSecondLayer(stOne(descent[:,1:1,end:end]),xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer of the Cylinder fit")
savefig("gradientFigures/cylinder1FitClassificationLocs.pdf")
plotSecondLayer(res[:,5],xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer original")
savefig("gradientFigures/cylinder1thatWasFit.png")
aveInScatSpace = mean(res[:,1:100])
plotSecondLayer(aveInScatSpace,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space cylinders")
savefig("gradientFigures/averageCylinderNormalized.pdf")
aveBell = mean(res[:,101:200])
plotSecondLayer(aveBell,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space Bells")
savefig("gradientFigures/averageBellNormalized.pdf")

aveFunnel = mean(res[:,201:300])
plotSecondLayer(aveFunnel,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space Funnels")
savefig("gradientFigures/averageFunnelNormalized.png")

plotSecondLayer(aveBell - aveFunnel,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space difference Bells-Funnels")

plotSecondLayer(aveInScatSpace,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space cylinders")
savefig("gradientFigures/averageCylinderNormalized.pdf")

aveInScatSpace = mean(res)
plotSecondLayer(aveInScatSpace,xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in Scat space full set")

plotSecondLayer(stOne(mean(dat,dims=3)),xVals=(.0005, .886),yVals=(0.0, .995),title="Second Layer mean in normal full set")
plot(mean(dat,dims=3)[:,1,1])

findfirst(oneAta.indices[3].!= cylLocs.indices[3]) #
testLoc = pathLocs{3}((nothing,nothing,falses(size(cylLocs.indices[3]))))
testLoc.indices[3][28,1,22,1] = true
testLoc.indices[3]
oneAta.indices[3]
y, back = pullback(x->stOne(x)[testLoc][end], descent[:,:,end:end])
back(y)
maximum(res[:,5:5][2])
maximum(stOne(descent[:,:,end:end])[2])
w = res[:,5:5][testLoc]
y, back = pullback(x->abs.(x[testLoc] - res[:,5:5][testLoc]), stOne(descent[:,:,end:end]))
maximum(abs.(back(y)[1][2]))
Zygote.refresh()
plot([stOne(descent[:,:,end:end])[oneAta] res[:,5:5][oneAta]])
specRes = res[:,5:5][oneAta]
y, back = pullback(x->abs.(x[oneAta] - specRes), stOne(descent[:,:,end:end]))
maximum(abs.(back(y)[1][2])) # function is actually zero, so this makes sense
plot([res[:,5:5][oneAta], stOne(descent[:,:,end:end])[oneAta]])
y, back = pullback(x->(res[:,5:5][oneAta][end]-stOne(x)[oneAta][end])^2, descent[:,:,end:end])
maximum(abs.(back(y)[1][2]))
maximum(abs.(gradient(x->obj(x,testLoc), descent[:,:,end:end])[1]))
maximum(abs.(gradient(x->obj(x,oneAta), descent[:,:,end:end])[1]))
plot(gradient(x->obj(x,testLoc), descent[:,:,end:end])[1][:,1,1])
function dem(x)
    @show norm(x)
    return x
end
function tmpObj(x,path)
    zippy = zip(stOne(x)[path],res[:,5:5][path])
    sum(Zygote.hook(dem, mse(a[1],a[2])) for a in zippy)
end
plot(gradient(x->tmpObj(x,oneAta), descent[:,:,end:end])[1][:,1,1])

[res[:,5:5][oneAta] stOne(descent[:,:,end:end])[oneAta]]
obj(descent[:,:,end:end], oneAta)
plot(gradient(x->obj(x,oneAta), descent[:,:,end:end])[1][:,1,1])
heatmap(descent[:,1,1:364])
plot(descent[:,1,364:364])
descent[:,:,end:end]+= .00001*norm(descent[:,1,end:end])*randn(128)
plot(log.(err[1:364]), legend=false)
heatmap(descent[:,1,:])
maximum(abs.(gradient(x->obj(x,oneAta), descent[:,:,end-1000:end-1000])[1]))

plot(gradient(x->obj(x,oneAta), descent[:,:,end:end]+.00001*norm(descent[:,1,end:end])*randn(128))[1][:,1,1])
obj(descent[:,:,end:end],oneAta)
plot([descent[:,1,end:end] descent[:,1,end:end]+.00001*norm(descent[:,1,end:end])*randn(128)])
plot([res[:,5:5][oneAta] stOne(descent[:,:,end:end]+.0001*norm(descent[:,1,end:end])*randn(128))[oneAta]],legend=false)

descent, err = continueTrain(N, descent, err; opt=opt, obj=(x->obj(x,oneAta)), keepWithin=10*norm(testSt[:,5]));plot(log.(err), legend=false)
pathsLeft(oneAta,cylLocs)
function pathsLeft(addTo,addFrom) 
    λ(x::Nothing,y::Nothing) = 0
    λ(x,y) = count(x.!=y)
    sum(map(λ,addTo.indices,addFrom.indices))
end
gradient(x->obj(x,oneAta), descent[:,:,1:1])
heatmap(descent[:,1,:])
gradient(x->obj(x,oneAta), descent[:,:,end-1:end-1])
42840/85/28
isnan(3.0)
count(isnan.(randn(30)))
colon()
fill(x->Colon(),3)
oneAtatmp = addNextPath(cylLocs)


# fitting just the coordinates directly
cylLocs = nonZeroPaths(coefs[:,1:1], allTogetherInOne=true,wholePath=false);
cylIntAdj = coefs[:,1:1]
cylIntAdj = broadcast(-, coefs[:,1:1], C[1]) # adjust by the intercept, and normalize
cylIntAdj = collatingTransform.normalize(cylIntAdj,1)
norm(cylIntAdj[cylLocs])
norm(cylIntAdj[2])
plot(cylIntAdj[cylLocs])
obj = makeObjFun(cylIntAdj, cylLocs, stOne, false)
norm(cylIntAdj)
initGuess = randn(128,1,1)
cylIntAdj[:,1:1][cylLocs]
stOne(initGuess)[cylLocs]
obj(initGuess)

# in this case, starting at the gradient evaluated at the object worked better. Sometimes
y, back = pullback(stOne, initGuess)
initGuess = back(cylIntAdj)[1];
initGuess = initGuess/norm(initGuess) * norm(cylIntAdj)/nPaths
initGuess = initGuess/norm(initGuess)
plot(initGuess[:,1,:])
plot([cylIntAdj[cylLocs] stOne(initGuess)[cylLocs]], title="coordinates in a random order",labels=["target" "current"])
nPaths = sum([prod(size(x)[2:end-1]) for x in cylIntAdj.result])
opt = Optimiser(InvDecay(1e-3), ADAM(.001)); N = 1000
opt = ADAM(.001); N = 100
descent, err = fitReverseSt(N, initGuess; opt=opt, obj=obj,keepWithin=1); plot!(log.(err))
opt
k = 8; plot(log.([mean(err[i:i+k]) for i=1:length(err)-k]))
savefig("/home/dsweber/WAT.png")
plot(descent[:,1,1:200:end], legend=false)
descent, err = continueTrain(N, descent, err; opt=opt, obj=obj, keepWithin=1); plot(log.(err))
opt = ADAM(.0001); N = 10000
opt = Momentum(.0000001); N = 100
opt = Descent(.0000001); N = 100
descentTmp, errTmp = continueTrain(N, descent, err; opt=opt, obj=obj); plot(log.(errTmp))
descent, err = continueTrain(N, descentTmp, errTmp; opt=opt, obj=obj);
plot([cylIntAdj[cylLocs] stOne(descent[:,:,end:end])[cylLocs]], title="coordinates in a random order",labels=["target" "current"])

longTrain, longTrainErr = (descent,err);


plot([norm(x) for x in eachslice(descent,dims=3)])
nPaths = sum([prod(size(x)[2:end-1]) for x in cylIntAdj.result])
size(descent)
obj(descent[:,:,end:end])
sum(mse(a[1],a[2]) for a in zip(stOne(descent[:,:,end:end])[cylLocs],cylIntAdj[cylLocs]))
abs(norm(descent[:,:,end:end]) - norm(cylIntAdj)/nPaths)
heatmap(descent[:,1,2:end])
plot(descent[:,1,:], legend=false)
plot(log.(abs.([cylIntAdj[cylLocs] stOne(descent[:,:,end:end])[cylLocs] stOne(descent[:,:,20:20])[cylLocs]])), title="coordinates in a random order",labels=["target" "current"])
# plotting scattering domain
plot([xp[1][:,1,1], yp[1][:,1,1], zp[1][:,1,1]], 
     labels=[firstLabel secondLabel thirdLabel], title="layer 2")
pnts = [71]
hline!(st(pathOfDescent[:,1:1,end:end])[p],label="")
scatter!(pnts, fill(min(minimum(xp[1]), minimum(yp[1]),
                             minimum(zp[1])),length(pnts)),label="fit points")


reshape(cylIntAdj[2])
cat(eachslice(cat(eachslice(wef, dims=Nd+2)..., dims=1), dims=Nd+2)...,dims=2)
function plotLayer2(sct)
    

end



#############################################################################
#############################################################################
# cone of influence stuff
#############################################################################
#############################################################################
function generateScatteredExample(y, p::pathLocs, value=1.0)
    grad = scattered(map(x->zeros(x...), size(y))); 
    grad[p] = value;
    grad    
end
generateScatteredExample(y) = scattered(map(x->zeros(x...), size(y))); 

#############################################################################
# what does the pushforward of the pullback look like? Nothing worthwhile, apparently
#############################################################################
y, back = pullback(stOne, dat[:,:,1:1])
l=21; s=10; δ = generateScatteredExample(y,pathLocs(1,(l, s,)))
δx = back(δ)[1]
plot(δx[:,1,1])
δy = stOne(δx)
heatmap(δy[pathLocs(1,:)][:,:,1]', xlabel="location", ylabel="freq index",
        title="eval of the gradient of st, layer 1 scale 10\n first layer") # generally reflects the location well
size(δy[pathLocs(2,:)])
size(δy[pathLocs(2,(:,1))])

k = 10; heatmap((δy[pathLocs(2,(:,k))][:,:,1])',
        title="eval of the gradient of st, layer 1 scale $s, location $l\n first layer $k",
               xlabel="location",ylabel="second layer freq index")
# what's a bit surprising is that the response is mostly towards the front,
# except the first pixel

k = 7; heatmap(((δy[pathLocs(2,(k,:))][:,:,1])'),
        title="eval of the gradient of st, layer 2 scale  $s, location $l\n second layer $k",
               xlabel="location",ylabel="first layer freq index")

# specifically forward of pullback of second layer?
y, back = pullback(stOne, initGuess)
δ2 = generateScatteredExample(y,pathLocs(2,(28,8,10)))
δ2x = back(δ2)[1];
plot(δ2x[:,1,1])
δ2y = stOne(δ2x)
heatmap(δ2y[pathLocs(1,:)][:,:,1]', xlabel="location", ylabel="freq index",
        title="eval of the gradient of st, layer 1 scale 10\n first layer")
# as you might expect, the first layer mostly responds to the path's location
# with a bit more emphasis on the lower frequency compared to the first 
# fix first layer, vary second
k = 10; heatmap((δ2y[pathLocs(2,(:,k))][:,:,1])',
        title="eval of the gradient of st, layer 1 scale $s, location $l\n first layer $k",
               xlabel="location",ylabel="second layer freq index")
# surprising amount concentrated in the lowest frequency, 8 isn't descernable

# fix second layer, vary first
k = 1; heatmap(((δ2y[pathLocs(2,(k,:))][:,:,1])'),
        title="eval of the gradient of st, layer 2 scale  $s, location $l\n second layer $k",
               xlabel="location",ylabel="first layer freq index")

y, back = pullback(stOne, initGuess)
x, ∂b = pullback(back, y)


#############################################################################
# what does the gradient look like?
#############################################################################
plot(dat[:,1,5])
y, back = pullback(stOne, dat[:,:,5:5])
origLoc1 = zeros(128,42,28);
typeof()
ndims(y)
∇cyl = collatingTransform.∇st(stOne, dat[:,:,5:5])
∇bell = collatingTransform.∇st(stOne, dat[:,:,115:115])
∇fun = collatingTransform.∇st(stOne, dat[:,:,203:203])
save("gradientExamples.jld2", "∇fun", ∇fun, "∇cyl", ∇cyl, "∇bell", ∇bell)

@load "gradientExamples.jld2"
cyl =dat[:,:,5]; bel = dat[:,:,115]; fun = dat[:,:,203];
plot(dat[:,1,[10,150,224]],labels=["cylinder" "bell" "funnel"],layout=(3,1))
savefig("gradientFigures/examples.png")
j=6
similar(stOne(dat[:,:,5:5]))
origLoc = collatingTransform.∇st(stOne, dat[:,:,5:5],1)

j = 26
plotFirstLayer1D(j, ∇cyl, dat[:,:,5])
plotFirstLayer1D(j, ∇bell, dat[:,:,115])
plotFirstLayer1D(j, ∇fun, dat[:,:,203])
savefig("gradientFigures/firstLayerGradient$j.png")

gifFirstLayer(∇, dat[:,:,5])

origLoc1[:,14,:]
plot(origLoc[:,21,:], line_z=(28:-1:1)', legend=false,color=:cividis, title="first layer gradient varying scale")

# second layer

size(wave1)
# colors: cividis
loc = (1,11) # layer 2, layer 1

plotSecondLayer1D((4,18), ∇, wave1, wave2, dat[:,1,5]) # this isn't ever
# really sensitive to things on the plateau

plotSecondLayer1D((4,12), ∇cyl, wave1, wave2, cyl)
plotSecondLayer1D((4,12), ∇bell, wave1, wave2, bel)
plotSecondLayer1D((4,12), ∇fun, wave1, wave2, fun)
cyl
for i=1:3,j=1:4
    println("i=$i,j=$j")
end
plotSecondLayer1D((4,28), ∇cyl, wave1, wave2, cyl)
plotSecondLayer1D((18,3), ∇cyl, wave1, wave2, cyl)
plotSecondLayer1D((18,15), ∇bell, wave1, wave2, bel)
plotSecondLayer1D((18,15), ∇fun, wave1, wave2, fun)
plotSecondLayer1D((15,18), ∇cyl, wave1, wave2, cyl)
plotSecondLayer1D((18,15), ∇cyl, wave1, wave2, cyl)
plotSecondLayer1D((1,15), ∇cyl, wave1, wave2, cyl)
savefig("gradientFigures/secondLayerGrad1_15_bel.png")
plotSecondLayer1D((1,15), ∇fun, wave1, wave2, fun)
savefig("gradientFigures/secondLayerGrad1_15_fun.png")
k2=4; k1 = 25
plotSecondLayer1D((k2,k1), ∇bell, wave1, wave2, bel)
savefig("gradientFigures/secondLayerGrad$(k2)_$(k1)_bel.png")
plotSecondLayer1D((k2,k1), ∇fun, wave1, wave2, cyl)
savefig("gradientFigures/secondLayerGrad$(k2)_$(k1)_cyl.png")

plotSecondLayer1D((1,11), ∇cyl, wave1, wave2, cyl) #used by all
plotSecondLayer1D((1,11), ∇bell, wave1, wave2, bel) #used by all
plotSecondLayer1D((1,11), ∇fun, wave1, wave2, fun) #used by all, can't really
#tell what's going on

plotSecondLayer1D((4,22), ∇, wave1, wave2) # one of the paths actually used
plotSecondLayer1D((9,3), ∇, wave1, wave2)  # jagged, only in one waveLoc
plotSecondLayer1D((18,9), ∇, wave1, wave2) # for some reason 9 in the first layer behaves rather
# strangely, with a strong presence at waveLoc 23 and
plotSecondLayer1D((18,10), ∇, wave1, wave2) # compare with, which has a weaker echo at 15
plotSecondLayer1D((18,11), ∇, wave1, wave2) # also at 11 for 18
plotSecondLayer1D((16,9), ∇, wave1, wave2)
plotSecondLayer1D((1,16), ∇, wave1, wave2)
plotSecondLayer1D((5,13), ∇, wave1, wave2)
# let's look at the diagonal (by matching the effective frequency)
diag = [argmin(abs.(meanWave(wave1) .- x))[2] for x in meanWave(wave2)]
diag = [zip(1:length(diag),diag)...]
plotSecondLayer1D(diag[18])
plotSecondLayer1D(diag[1]) # moves backwards? 
plotSecondLayer1D((2,12))

size(origLoc)
waveLoc=15; fLInd = 10; heatmap(origLoc[:,waveLoc,3:end,fLInd]',xlabel="space",
                                ylabel="second layer index") # there tends to
# be a location 

# second layer index by space
waveLoc=1; fLInd = 18;
heatmap(origLoc[:,waveLoc,:,fLInd]',xlabel="space",ylabel="second layer index",title="second layer gradient\n location $waveLoc, first layer $fLInd")
anim = Animation()
size(origLoc)
for waveLoc=2:28
    heatmap(origLoc[:,waveLoc,:,fLInd]',xlabel="space",ylabel="second layer index",title="second layer gradient\n location $waveLoc, first layer $fLInd")
    frame(anim)
end
gif(anim,"gradientFigures/varySecond_first$(fLInd).gif",fps=2)

# first layer index by space
waveLoc=1; sLInd = 10;
heatmap(origLoc[:,waveLoc,sLInd,:]',xlabel="space",ylabel="first layer index",title="first layer gradient\n location $waveLoc, second layer $fLInd")
anim = Animation()
size(origLoc)
for waveLoc=2:28
    heatmap(origLoc[:,waveLoc,sLInd,:]',xlabel="space",ylabel="first layer index",title="second layer gradient\n location $waveLoc, second layer $sLInd")
    frame(anim)
end
gif(anim,"gradientFigures/varyFirst_second$(sLInd).gif",fps=1)
# not sure if there's something strange going on with the normalization, but
# the gradient is strangely quite large for the highest frequencies in the
# first layer, making it hard to compare for a fixed index in the second layer


heatmap(origLoc[:,waveLoc,3:end,fLInd]',xlabel="space",ylabel="second layer index",title="second layer gradient\n location $waveLoc, first layer $fLInd") # there tends to
# be a location 




heatmap(origLoc[:,:,1,1], xlabel="wavelet location", ylabel="space", legend=false,color=:cividis, title="second layer gradient varying location, path (1,1)")

heatmap(origLoc[:,6,:,1], xlabel="wavelet location", ylabel="space", color=:cividis, title="first layer gradient varying scale")




k = 30;tmp = back(stOne(dat[:,:,k:k]))[1][:,1,1]
plot([dat[:,1,k] tmp .* norm(dat[:,1,k])./ norm(tmp,1)])


gradient(t->sin(t^2),0)
yy, ∂ = pullback(t->sin(t^2),0)
∂(03000)
w =map((x,t)->x(t)[1], [pullback(sin, x)[2] for x in range(0, stop=8π, length=1000)], range(0, stop=8π, length=1000))
w =map((x)->x(1)[1], [pullback(sin, x)[2] for x in range(0, stop=8π, length=1000)])
plot([w sin.(range(0, stop=8π, length=1000))])
plot([x[1] for x in ∂.(range(0,stop=2π,length=1000))])
∂(1000)
