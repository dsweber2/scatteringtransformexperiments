using Revise
using Flux, CUDA, FFTW
using collatingTransform
using Test
using MLDatasets
using HDF5
using Base.GC

train_x, train_y = CIFAR10.traindata()
test_x, test_y = CIFAR10.testdata()
tot_x = Float32.(cat(train_x, test_x, dims=4))
#tot_x = reshape(tot_x, (size(tot_x)[1:2]..., 1, size(tot_x,3)))
tot_y = cat(train_y, test_y, dims=1)


altSlopes(x) = x>0 ? x : x/2
nonlins = ((tanh, "tanh"),) #(abs, "abs"), (altSlopes, "altSlopes"), (relu, "relu"), 

for (transform, named) in nonlins
    GC.gc()
    # create the transform
    batchSize = 10
    @time sst = scatteringTransform((size(tot_x)[1:3]..., batchSize), 2,
                                    σ=transform);

    @time res = sst(cu(tot_x[:, :, :, 1:batchSize]));
    @time tmp = flatten(res); size(tmp)
    typeof(tmp)
    size(res[1])
    nTot = length(tot_y)
    totalBatches = ceil.(nTot/batchSize)
    tot_trans = zeros(Float32, size(tmp,1), size(tot_x)[end]);
    a = time();
    start = time();
    # do the actual transform
    for i=1:Int(totalBatches)
        res = sst(cu(tot_x[:,:,:, (1 + (i-1)*batchSize):i*batchSize]));
        tot_trans[:,(1 + (i-1)*batchSize):i*batchSize] = flatten(res);
        if i%100==0
            println("on i=$(i), taking $(time()-a), total time of $(time()-start)")
            a=time()
        end
    end
    
    # write to disk
    h5open("/fasterHome/workingDataDir/shattering/CIFAR10/collatingVersion.h5", "cw") do file
        write(file, "poolBy65/$(named)", tot_trans)
    end
end



h5open("/fasterHome/workingDataDir/shattering/CIFAR10/Labels.h5", "cw") do file
    write(file, "labels", tot_y)
end

needToWriteNull = true
if needToWriteNull
    h5open("/fasterHome/workingDataDir/shattering/CIFAR10/collatingVersion.h5", "cw") do file
        write(file, "poolBy65/noTransform", reshape(tot_x, (28*28*1, :)))
    end
end
